/* eslint-disable prettier/prettier */
var baseUrl =
  'http://levolevo-dev.eba-vkgeczjp.us-east-2.elasticbeanstalk.com/';

export async function register(payLoad) {
  var myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };
  return fetch(baseUrl + 'api/v1/user/register', requestOptions)
    .then((response) => {
      console.log(response.status, 'pp');
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function login(payLoad) {
  var myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };
  return fetch(baseUrl + 'api/v1/user/login', requestOptions)
    .then((response) => {
      return response;
    })
    .then(async (result) => {
      if (result.status == 200) {
        console.log(result.headers.map['auth-token'], '====');

        return {
          id: await result.json(),
          token: result.headers.map['auth-token'],
        };
      } else {
        return result.text();
      }
    })
    .catch((error) => console.log('error', error));
}

export async function ForgotPassword(payLoad) {
  var myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };
  return fetch(baseUrl + 'api/v1/recover/forgot-password', requestOptions)
    .then((response) => response.text())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}
export async function AddOrder(payLoad, ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'multipart/form-data');
  var raw = payLoad;
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };
  return fetch(baseUrl + 'api/v1/order/' + ids.uId, requestOptions)
    .then((response) => {
      console.log('=&&=', response.status);
      if (response.status == 200) {
        return response.json();
      } else {
        console.log('res', response.status);
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function getOrders(ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,

    redirect: 'follow',
  };
  return fetch(baseUrl + 'api/v1/order/' + ids.uId, requestOptions)
    .then((response) => {
      console.log('=&&=', response.status);
      if (response.status == 200) {
        return response.json();
      } else {
        console.log('res', response.status);
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}
export async function getAirports() {
  var myHeaders = new Headers();

  myHeaders.append('Content-Type', 'multi');

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  return fetch(baseUrl + 'api/v1/airport/', requestOptions)
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function AddTrip(payLoad, ids) {
  console.log(payLoad, 'data');
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  return fetch(baseUrl + 'api/v1/trip/' + ids.uId, requestOptions)
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        console.log('res', '+++');
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function UpdateTrip(payLoad, ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/trip/' + ids.uId + '/' + ids.tripId,
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function DeleteTrip(ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var requestOptions = {
    method: 'DELETE',
    headers: myHeaders,
    body: '',
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/trip/' + ids.uId + '/' + ids.tripId,
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 204) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function getTrips(id, token) {
  var myHeaders = new Headers();

  myHeaders.append('Content-Type', 'application/json');
  myHeaders.append('auth-token', token);
  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  return fetch(baseUrl + 'api/v1/trip/' + id, requestOptions)
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function updateTripViews(ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);

  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    // body: formdata,
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/trip/' + ids.uId + '/' + ids.tripId + '/views',
    requestOptions,
  )
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}
export async function deleteOrders(ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var requestOptions = {
    method: 'DELETE',
    headers: myHeaders,
    body: '',
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/order/' + ids.uId + '/' + ids.orderId,
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 204) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function UpdateOrder(payLoad, ids) {
  console.log(ids);
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'multipart/form-data');
  var raw = payLoad;
  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/order/' + ids.uId + '/' + ids.orderId,
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function getOffer(payLoad, ids) {
  console.log(payLoad, ids);
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/offer/' + ids.uId + '/' + ids.orderId,
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function addOffer(payLoad, ids) {
  console.log(payLoad, ids);
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  return fetch(
    baseUrl + 'api/v1/offer/' + ids.uId + '/' + ids.orderId,
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function acceptOffer(ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var payLoad = {
    accepted: true,
  };
  var raw = JSON.stringify(payLoad);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  return fetch(
    baseUrl +
      'api/v1/offer/' +
      ids.uId +
      '/' +
      ids.orderId +
      '/' +
      ids.offerId +
      '/approval',
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 200) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export async function rejectOffer(ids) {
  var myHeaders = new Headers();
  myHeaders.append('auth-token', ids.token);
  myHeaders.append('Content-Type', 'application/json');
  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    redirect: 'follow',
  };

  return fetch(
    baseUrl +
      'api/v1/offer/' +
      ids.uId +
      '/' +
      ids.orderId +
      '/' +
      ids.offerId +
      '/cancel',
    requestOptions,
  )
    .then((response) => {
      console.log(response);
      if (response.status == 204) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}
