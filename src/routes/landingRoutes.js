import LandingScreen from '../screens/landingScreen';
import Login from '../screens/login';
import Signup from '../screens/signup';
import ForgotPassword from '../screens/forgotPassword';
import ResetPassword from '../screens/resetPassword';
import Home from '../routes/bottomTab'
import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function LandingRoutes() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="LandingScreen" component={LandingScreen} options={{
            headerShown:false
        }} />
         <Stack.Screen name="Login" component={Login} options={{
            headerShown:false
        }} />
          <Stack.Screen name="signup" component={Signup} options={{
            headerShown:false
        }} />
            <Stack.Screen name="forgotPassword" component={ForgotPassword} options={{
            headerShown:false
        }} />
        <Stack.Screen name="resetPassword" component={ResetPassword} options={{
            headerShown:false
        }} />
         <Stack.Screen name="home" component={Home} options={{
            headerShown:false
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default LandingRoutes;

