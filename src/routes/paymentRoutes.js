import PaymentHistory from '../screens/paymentHistory';
import paymentRecevided from '../screens/paymentRecevide';
import PaymentSent from '../screens/paymentsent';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function PaymentRoutes() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="paymentHistory" component={PaymentHistory} options={{
            headerShown:false
        }} />  
         <Stack.Screen name="paymentRecevied" component={paymentRecevided} options={{
            headerShown:false
        }} />  
          <Stack.Screen name="paymentSent" component={PaymentSent} options={{
            headerShown:false
        }} />   
      </Stack.Navigator>
  );
}
export default PaymentRoutes;

