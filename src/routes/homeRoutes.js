import Home from '../screens/home';


import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function homeRoutes() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="home" component={Home} options={{
            headerShown:false
        }} />
       
      </Stack.Navigator>
  );
}
export default homeRoutes;

