import MyTrips from '../screens/mpTrips';
import EditTrip from '../screens/editTrip';
import TripOffers from '../screens/tripOffers';
import AddTrip from '../screens/addTrip'
import OrderDetail from '../screens/orderDetails'
import reviewOrderDetails from '../screens/reviewOrderDetails'
import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function tripRoutes() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="myTrips" component={MyTrips} options={{
            headerShown:false
        }} />
         <Stack.Screen name="addTrip" component={AddTrip} options={{
            headerShown:false
        }} />
          <Stack.Screen name="editTrip" component={EditTrip} options={{
            headerShown:false
        }} />  
          <Stack.Screen name="tripOffers" component={TripOffers} options={{
            headerShown:false
        }} /> 
         <Stack.Screen name="orderDetail" component={OrderDetail} options={{
            headerShown:false
        }} />  
          <Stack.Screen name="reviewOrderDetails" component={reviewOrderDetails} options={{
            headerShown:false
        }} />   
      </Stack.Navigator>
  );
}
export default tripRoutes;

