import OrderHome from '../screens/orderHome';
import AddOrder from '../screens/addOrder';
import ViewOffers from '../screens/viewOffers';
import ViewTravlers from '../screens/viewTravlers';
import reviewOrderDetails from '../screens/reviewOrderDetails';
import ReviewOffers from '../screens/reviewOffers';
import ReceivedOrders from '../screens/receviedOrders';
import RejectParcel from '../screens/rejectParcel';
import EditOrder from '../screens/editOrders';
import AddOffer from '../screens/addOffer';

import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function orderRoutes(props) {
  return (
      <Stack.Navigator>
        <Stack.Screen name="orderHome" component={OrderHome} options={{
            headerShown:false
        }} />
        <Stack.Screen name="addOffer" component={AddOffer} options={{
            headerShown:false
        }} />
        <Stack.Screen name="addOrder" component={AddOrder} options={{
            headerShown:false
        }} />
        <Stack.Screen name="editOrder" component={EditOrder} options={{
            headerShown:false
        }} />
        <Stack.Screen name="viewOffers" component={ViewOffers} options={{
            headerShown:false
        }} />
         <Stack.Screen name="viewTravlers" component={ViewTravlers} options={{
            headerShown:false
        }} />
         <Stack.Screen name="reviewOrderDetails" component={reviewOrderDetails} options={{
            headerShown:false
        }} />
        <Stack.Screen name="reviewOffers" component={ReviewOffers} options={{
            headerShown:false
        }} />
        <Stack.Screen name="receivedOrder" component={ReceivedOrders} options={{
            headerShown:false
        }} />
        <Stack.Screen name="rejectParcel" component={RejectParcel} options={{
            headerShown:false
        }} />
      </Stack.Navigator>
  );
}
export default orderRoutes;

