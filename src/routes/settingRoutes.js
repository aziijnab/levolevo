import Setting from '../screens/setting';
import Profile from '../screens/profile';
import PaymentHistory from '../routes/paymentRoutes';
import Wallet from '../screens/wallet';
import InviteFriends from '../screens/inviteFriends';
import Terms from '../screens/terms'

import * as React from 'react';

import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function SettingRoutes() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="settingHome" component={Setting} options={{
            headerShown:false
        }} />  
        <Stack.Screen name="profile" component={Profile} options={{
            headerShown:false
        }} /> 
         <Stack.Screen name="payments" component={PaymentHistory} options={{
            headerShown:false
        }} />  
        <Stack.Screen name="wallet" component={Wallet} options={{
            headerShown:false
        }} />  
         <Stack.Screen name="inviteFriends" component={InviteFriends} options={{
            headerShown:false
        }} />  
        <Stack.Screen name="terms" component={Terms} options={{
            headerShown:false
        }} /> 
      </Stack.Navigator>
  );
}
export default SettingRoutes;

