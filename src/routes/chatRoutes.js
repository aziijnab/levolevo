import ChatHome from '../screens/chatHome';


import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

function homeRoutes() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="chatHome" component={ChatHome} options={{
            headerShown:false
        }} />
       
      </Stack.Navigator>
  );
}
export default homeRoutes;

