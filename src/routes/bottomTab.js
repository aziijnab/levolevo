//import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import * as React from 'react';
import Home from '../routes/homeRoutes';
import Trips from '../routes/tripRoutes'
import Setting from '../routes/settingRoutes';
import Order from '../routes/orderRoutes';
import Chat from '../routes/chatRoutes'


import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Material from 'react-native-vector-icons/MaterialIcons';

const Tab = createMaterialBottomTabNavigator();

export default function App() {
    return (

        <Tab.Navigator
            labeled={true}
            shifting={false}
            activeColor="#0D3B66"
            initialRouteName="Home"
            barStyle={{ backgroundColor: '#FFFFFF', height: 60 }}>
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarLabel: "Home",
                    tabBarVisible: false,
                    tabBarIcon: ({ color, focused }) => (
                        <FontAwesome name="home" color={color} size={26} />

                    ),
                }}
            />
            <Tab.Screen
                name="Orders"
                component={Order}
                options={{
                    tabBarLabel: "Orders",
                    tabBarVisible: false,
                    tabBarIcon: ({ color, focused }) => (

                        <FontAwesome5 name="box" color={color} size={26} />

                    ),
                }}
            />
            <Tab.Screen
                name="Trips"
                component={Trips}
                options={{
                    tabBarLabel: "Trips",
                    tabBarVisible: false,
                    tabBarIcon: ({ color, focused }) => (
                        <FontAwesome name="plane" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Messages"
                component={Chat}
                options={{
                    tabBarLabel: "Messages",
                    tabBarVisible: false,
                    tabBarIcon: ({ color, focused }) => (
                        <Material name="chat" color={color} size={26} />
                    ),
                }}
            />
             <Tab.Screen
                name="Settings"
                component={Setting}
                options={{
                    tabBarLabel: "Settings",
                    tabBarVisible: false,
                    tabBarIcon: ({ color, focused }) => (
                        <Material name="settings" color={color} size={26} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}
