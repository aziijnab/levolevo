export default
{
"trip": {
    "createdOn": "2021-01-19T02:20:20.949Z",
    "views": 0,
    "status": "new",
    "lastModifiedOn": "2021-01-19T02:20:20.949Z",
    "_id": "6006465bd17686f2c025848b",
    "origin": "sao",
    "dest": "sdu",
    "date": "2021-01-21",
    "time": "22:00",
    "bids": [
        {
            "status": "rejected",
            "createdOn": "2021-01-19T02:20:20.949Z",
            "lastModifiedOn": "2021-01-19T02:20:20.949Z",
            "_id": "600646a4d17686f2c025848d",
            "orderId": "5ffd15db3564c4e0659967a2",
            "offerId": "600646a3d17686f2c025848c"
        }
    ]
},
"orders": [
    {
        "senderName": "Filipe Pinto",
        "senderStarRank": 0,
        "orderId": "5ff674f4cab628cc2ce43079",
        "createdOn": "2021-01-07T02:36:45.932Z",
        "lastModifiedOn": "2021-02-19T00:49:38.552Z",
        "views": 0,
        "status": "handedOver",
        "origin": "sao",
        "dest": "sdu",
        "date": "2021-01-20",
        "dateMax": "2021-01-21",
        "description": "comida",
        "weight": "0.1",
        "dimension": "10 x 1 x 5",
        "image": "uploads/orders/2021-01-07T02:41:56.515Z-IMG.jpg"
    },
    {
        "senderName": "Filipe Pinto",
        "senderStarRank": 0,
        "orderId": "601cef7c4fb0161d1f9f4360",
        "createdOn": "2021-02-04T04:21:56.711Z",
        "lastModifiedOn": "2021-02-04T04:21:56.711Z",
        "views": 0,
        "status": "new",
        "origin": "sao",
        "dest": "sdu",
        "date": "2021-01-20",
        "dateMax": "2021-01-21",
        "description": "comida",
        "weight": "2",
        "dimension": "10 x 20 x 10",
        "image": "uploads/orders/2021-02-05T07:10:52.711Z-IMG.jpg"
    },
    {
        "senderName": "Filipe Pinto",
        "senderStarRank": 0,
        "orderId": "601d30a196a17830f037c946",
        "createdOn": "2021-02-05T11:47:34.467Z",
        "lastModifiedOn": "2021-02-05T11:47:34.467Z",
        "views": 0,
        "status": "new",
        "origin": "sao",
        "dest": "sdu",
        "date": "2021-01-21",
        "dateMax": "2021-01-22",
        "description": "comida2",
        "weight": "2",
        "dimension": "10 x 20 x 10",
        "image": "uploads/orders/2021-02-05T11-48-49.586Z-IMG.jpg"
    },
    {
        "senderName": "Filipe Pinto",
        "senderStarRank": 0,
        "orderId": "601d3f2fbcf7cd37419a0a2f",
        "createdOn": "2021-02-05T12:50:28.700Z",
        "lastModifiedOn": "2021-02-05T12:50:28.700Z",
        "views": 0,
        "status": "new",
        "origin": "sao",
        "dest": "sdu",
        "date": "2021-01-21",
        "dateMax": "2021-01-22",
        "description": "comida3",
        "weight": "2",
        "dimension": "10 x 20 x 10",
        "image": "/Users/lfssp/projects/levolevo/backend/uploads/orders/2021-02-05T12-50-55.567Z-IMG.jpg"
    },
    {
        "senderName": "Filipe Pinto",
        "senderStarRank": 0,
        "orderId": "6032f75ad1f2e334b5e9e9e3",
        "createdOn": "2021-02-22T00:13:15.226Z",
        "lastModifiedOn": "2021-02-22T00:13:15.226Z",
        "views": 0,
        "status": "new",
        "origin": "sao",
        "dest": "sdu",
        "date": "2021-01-20",
        "dateMax": "2021-01-21",
        "description": "comida5",
        "weight": "2",
        "dimension": "10 x 20 x 10",
        "image": "https://levolevo-imgs-dev.s3.sa-east-1.amazonaws.com/orders/1613952853522IMG.jpg"
    }
],
"matchingOrders": 5
}
