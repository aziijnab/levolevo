import React, { Component } from "react";
import { StyleSheet, View, StatusBar } from "react-native";
import { Header  } from 'react-native-elements'

class header extends Component {
    render() {
        return (
            <View >
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header
                    leftComponent={{ icon: 'ios-chevron-back', type: 'ionicon', color: '#0D3B66',onPress:()=>{this.props.back.goBack()} }}
                    centerComponent={{ text: this.props.title, style: { fontSize: 20, fontWeight: '600' ,color:'#0D3B66'} }}
                    containerStyle={styles.header}
                />
          </View>
        )
    }
}
const styles = StyleSheet.create({
    header: { backgroundColor: '#fff' },
});
export default header