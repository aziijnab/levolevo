import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity, ScrollView } from "react-native";
import { Icon, ListItem, Avatar } from 'react-native-elements'
import { Picker } from '@react-native-community/picker';
const { width, height } = Dimensions.get('screen')
class myOrders extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: 'sort by status',
        }
    }
    render() {

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />

                <ScrollView style={{ margin: 10 }}>
                    <View style={{
                        borderColor: 'silver', borderWidth: 1, marginTop: 10, shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 12,
                        },
                        shadowOpacity: 0.58,


                        elevation: 5,
                    }}>


                        <Picker
                            selectedValue={this.state.days}
                            mode={'dropdown'}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ days: itemValue })
                            }>
                            <Picker.Item label="sort by status" value="1" />
                            <Picker.Item label="sort by origin" value="2" />
                            <Picker.Item label="sort by destination city" value="2" />
                        </Picker>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    row: { flexDirection: 'row', justifyContent: 'space-between', width: width - 50, marginBottom: 10 },
    amount: { color: '#F95738', fontSize: 18 },
    date: { color: '#939393', fontSize: 13 }
});
export default myOrders