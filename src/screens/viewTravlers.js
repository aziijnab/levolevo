import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating } from 'react-native-elements'

const { width, height } = Dimensions.get('screen')
class viewTravlers extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            travelersList: this.props.route.params?.travelersList ?? '',
            orderId: this.props.route.params?.orderId ?? '',
            origin: this.props.route.params?.origin ?? '',
            dest: this.props.route.params?.dest ?? '',
            date: this.props.route.params?.date ?? '',
            matches: this.props.route.params?.matches ?? 0,
            subArry:[require('../assets/p1.png'),require('../assets/p2.png')]
        }
        // console.log("this.props.route.params")
        // console.log(this.props.route.params)
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header
                   // rightComponent={{ icon: 'plus', type: 'font-awesome', color: '#F95738', onPress: () => { this.props.back.goBack() } }}
                    centerComponent={{ text: 'View Travlers', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />
                <ScrollView>

               <View >
                    <View style={styles.tripHeader}>
                        <View>
                            <View style={styles.row}>
                                <Text style={styles.heading}>{this.state.origin}</Text>
                                <Icon type="material" name="arrow-right-alt" />
                                <Text style={styles.heading}>{this.state.dest}</Text>
                            </View>
                            <Text style={styles.date}>{this.state.date}</Text>
                        </View>

                    </View>
                    <Text style={styles.matchesContainer}>{this.state.matches} matches</Text>
                    {this.state.travelersList.length > 0 ?
                        this.state.travelersList.map((item,index)=>(
                        <View key={index}>
                        <ListItem>
                            <Avatar source={item.travelerProfileImage ? item.travelerProfileImage : require("../assets/placeholder.png")} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                            <ListItem.Content>
                                <ListItem.Title>{item.travelerName}</ListItem.Title>
                                <Rating
                                    imageSize={15}
                                    startingValue={item.travelerStarRank}
                                    ratingCount={5}
                                    style={{ paddingVertical: 10 }}
                                />
                                <View style={styles.row}>
<Text>Your bid</Text>
<Text style={{marginLeft:10,backgroundColor:'#F4F6FB',padding:10}}>$39 </Text>
                                </View>
                                <Button
                                    title="Send an offer"
                                    buttonStyle={{ backgroundColor: "#F95738" }}
                                    containerStyle={styles.b1}
                                    // onPress={()=>{this.props.navigation.navigate('reviewOffers')}}
                                    onPress={() => {
                            this.props.navigation.navigate('addOffer',{orderId:this.state.orderId})
                          }}
                                />

                            </ListItem.Content>
                        </ListItem>
                    </View>
                    ))
                    :null }
                  </View>


                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    editIcon: { margin: 10 },
    matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
    b1: { width: (width-50) / 2, height: 30, justifyContent: 'center', marginTop: 10 }

});
export default viewTravlers
