import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Button, Image, Input } from 'react-native-elements'
import Header from '../components/header'

const { width, height } = Dimensions.get('screen')

import { UpdateOrder, getAirports, deleteOrders, updateTripViews } from '../backend/apis'
import DropDownPicker from 'react-native-dropdown-picker';

import AsyncStorage from '@react-native-community/async-storage';
import DateTimePickerModal from "react-native-modal-datetime-picker";

class editOrder extends Component {
    constructor(props) {
        super(props)
        this.state = {

            origin: null,
            dest: '',
            airports: [],
            token: '',
            userId: '',
            date: new Date(),
            showDate: false,
            showdateMax: false,
            dateMax: new Date(),
            description :this.props.route.params?.orderData.order.description ?? '',
            weight :this.props.route.params?.orderData.order.weight ?? '',
            dimension :this.props.route.params?.orderData.order.dimension ?? '',
            image:'http://levolevo-dev.eba-vkgeczjp.us-east-2.elasticbeanstalk.com/'+this.props.route.params?.orderData.order.prodImage ?? '',
            isDatePickerVisible: false,
            isDatePickerVisiblem: false,
            selectedOrigin: this.props.route.params?.orderData.order.origin ?? 'org',
            selectedDest: this.props.route.params?.orderData.order.dest ?? 'des',
            orderId: this.props.route.params?.orderData.order._id,
            status: 'active',
            views:0
        }
      
    }
    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true })
    };
    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false });
    };
    handleConfirmDatePicker = (date) => {
        let x = date
        var date = x.toISOString().slice(0, 10);
        this.setState({
            date: date
        })
        this.hideDatePicker();
    };

    showDatePickerm = () => {
        this.setState({ isDatePickerVisiblem: true })
    };
    hideDatePickerm = () => {
        this.setState({ isDatePickerVisiblem: false });
    };
    handleConfirmDatePickerm = (date) => {
        let x = date.todateMaxString().substr(0, 5)
        this.setState({ dateMax: x })
        this.hideDatePickerm();
    };
    async componentDidMount() {


        this.setState({
            date: this.props.route.params?.orderData.order.date ?? new Date(),
            dateMax: this.props.route.params?.orderData.order.dateMax ?? new Date(),
        })
        const token = await AsyncStorage.getItem('@token');
        if (token != null) { this.setState({ token: token }) }
        else { }
        const userId = await AsyncStorage.getItem('@userId');
        if (userId != null) { this.setState({ userId: userId }) }
        else { }

        //get and save views
          /*  try {
            let payLoad = {
                uId: this.state.userId,
                token: this.state.token,
                orderId: this.state.orderId
            }
    
        updateTripViews(payLoad).then(data => {
                //  
                if (typeof data == 'object') {
              
                    this.setState({
                        views: data.views
                    })
                }
                else {
                    alert(data+'p')
                }
            })
        } catch (e) {
            alert('=')
            // console.log(e)
        }*/
        try {

            getAirports().then(data => {
                //  
                if (typeof data == 'object') {
                    //alert(data.id)
                    let tempAirport = []
                    for (let i = 0; i < data.airports.length; i++) {
                        //    console.log(data.airports[i].IATA)
                        if (this.state.selectedOrigin == data.airports[i].IATA) {

                            this.setState({
                                origin: data.airports[i].City
                            })
                        }
                        if (this.state.selectedDest == data.airports[i].IATA) {

                            this.setState({
                                dest: data.airports[i].City
                            })
                        }
                        tempAirport.push({
                            label: data.airports[i].City,
                            value: data.airports[i].City,
                            IATA: data.airports[i].IATA,
                        })
                    }
                    this.setState({
                        airports: tempAirport
                    })
                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            alert('=')
            // console.log(e)
        }
    }

    UpdateOrder = () => {
        var formdata = new FormData();
        formdata.append(`origin`, this.state.selectedOrigin);
        formdata.append("dest", this.state.selectedDest);
        formdata.append("date", this.state.date);
        formdata.append("dateMax", this.state.dateMax);
        formdata.append("description", this.state.description);
        formdata.append("weight", this.state.weight);
        formdata.append("dimension", this.state.dimension);
        formdata.append("prodImage", {
            uri: this.state.image,
            type: 'image/jpeg', // or photo.type
            name: this.state.image
          })
        let payLoad = {
            uId: this.state.userId,
            token: this.state.token,
            orderId: this.state.orderId
        }


        try {
            UpdateOrder(formdata, payLoad).then(data => {
                //  
                if (typeof data == 'object') {
                    //alert(data.id)
                    //  alert('Successfully login ')
                    console.log(data)
                    this.props.navigation.navigate('orderHome', { value: 'x' })
                    // alert('trip Updated')
                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            console.log('=',e)
        }

    }

    deleteTrip = () => {

        let payLoad = {
            uId: this.state.userId,
            token: this.state.token,
            orderId: this.state.orderId
        }
        try {
            deleteOrders(payLoad).then(data => {
                console.log(data)
                this.props.navigation.navigate('orderHome', { value: 'x' })

            })
        } catch (e) {
            alert('=')
        }
    }
    render() {
        var { origin, dest, date, dimension, date, showDate, showdateMax, dateMax, selectedOrigin ,views,description,weight,image} = this.state
   
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header title="Orders" back={this.props.navigation} />
                <View style={{ alignItems: 'center' }}>
          <Image
            source={image == "" ? require('../assets/p1.png') : { uri: image }}
            style={{ width: width, height: 200 }}
            resizeMode={image == "" ? "cover" : "center"}
          />
          {
            image == "" ?
              <TouchableOpacity style={styles.uploadButton} onPress={this.showImagePicker}>
                <Text style={{ color: '#fff' }}>Upload photo</Text>
              </TouchableOpacity> :
              <TouchableOpacity style={styles.uploadButton} onPress={this.showImagePicker}>
                <Text style={{ color: '#fff' }}>Change photo</Text>
              </TouchableOpacity>
          }
        </View>
                <Text style={styles.secondLabel}>Views ({views})</Text>

                <ScrollView>
                    <Text style={styles.secondLabel}>From</Text>
                    <DropDownPicker
                        items={this.state.airports}

                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        placeholder={origin}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {
                            this.setState({
                                origin: item.IATA,
                                selectedOrigin: item.IATA
                            }, () => {
                            })
                        }}
                        searchable
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                    <Text style={styles.secondLabel}>To</Text>
                    <DropDownPicker
                        items={this.state.airports}
                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        placeholder={dest}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {
                            this.setState({
                                dest: item.IATA,
                                selectedDest: item.IATA
                            })
                        }}
                        searchable
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                     <Input
                        placeholder='Description'
                        label="Description"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={description}
                        onChangeText={(description)=>this.setState({description})}
                       
                    />
                    <Input
                        placeholder='Weight'
                        label="Weight"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={weight}
                        onChangeText={(weight)=>this.setState({weight})}
                       
                    />
                      <Input
                        placeholder='Dimesnion'
                        label="Dimesnion"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={dimension}
                        onChangeText={(dimension)=>this.setState({dimension})}
                       
                    />
                    <Input
                        placeholder='Date'
                        label="Date"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'calendar', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={date}
                        onFocus={this.showDatePicker}
                    />
                    <Input
                        placeholder='Date Max'
                        label="Date Max"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'clock-o', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={dateMax}
                        onFocus={this.showDatePickerm}
                    />
                  
                
                    {
                        //date picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisible}
                        mode="date"
                        onConfirm={this.handleConfirmDatePicker}
                        onCancel={this.hideDatePicker}
                    />
                    {
                        //time picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisiblem}
                        mode="date"
                        onConfirm={this.handleConfirmDatePickerm}
                        onCancel={this.hideDatePickerm}
                    />

                    <Button
                        title="Update"
                        buttonStyle={{ backgroundColor: '#F95738', margin: 20 }}
                        onPress={this.UpdateOrder}
                    />
                    <Button title="Delete Trip" type="outline" buttonStyle={{ margin: 10 }}
                        onPress={this.deleteTrip}
                    />
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '90%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle: { borderBottomWidth: 0 },
    labelStyle: { backgroundColor: '#fff', color: '#000', fontWeight: '300' },
    pcikerLabel: { marginLeft: 25, fontSize: 15, marginTop: 16 },
    uploadButton: { position: 'absolute', bottom: 10, backgroundColor: 'rgba(0,0,0,.5)', padding: 5, borderRadius: 10 },
    secondLabel: { fontSize: 18, marginLeft: '6%', marginTop: 16, fontWeight: '300' }

});
export default editOrder