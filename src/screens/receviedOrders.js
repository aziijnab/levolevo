import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, TouchableOpacity ,ScrollView} from "react-native";
import {   Button, Image  ,Header } from 'react-native-elements'
import Share from 'react-native-share';

const { width, height } = Dimensions.get('screen')
class ReceivedOrders extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                
                <Header
                   // rightComponent={{ icon: 'plus', type: 'font-awesome', color: '#F95738', onPress: () => { this.props.back.goBack() } }}
                    centerComponent={{ text: 'Recevied', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />
                <ScrollView>
                <Image
  source={require('../assets/Vector.png')}
  style={styles.image}
  containerStyle={{alignSelf:'center'}}
/>
<Button
  title="Share"
  buttonStyle={{backgroundColor:'#F95738'}}
  containerStyle={styles.b1}
  onPress={()=>{
    const shareOptions = {
        title: 'Share via',
        message: 'some message',
      
      };
      Share.open(shareOptions)
      .then((res) => { console.log(res) })
      .catch((err) => { err && console.log(err); });
  }}
/>
      </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    image:{width: 200, height: 200,margin:20}  ,
    b1:{width:width-50,alignSelf:'center'} 
});
export default ReceivedOrders