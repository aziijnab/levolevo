import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity ,ScrollView} from "react-native";
import { Button, Icon, Input } from 'react-native-elements'
import Header from '../components/header'
import {register} from '../backend/apis'
const { width, height } = Dimensions.get('screen')
class signupScreen extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            email:'gpo@gmm.com',
            password:'1234',
            confrmPassword:'m',
            firstName:'m',
            lastName:'aima'
        }
    }
    onRegister=()=>
    {
        let payLoad={
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            password: this.state.password
        }
        try{
            register(payLoad).then(data=>{
          
           console.log(typeof data)
           if(typeof data =='object')
           {
                alert(data.id)
           }
           else
           {
               alert(data)
           }
            })
             }catch(e)
             {
                 alert('=')
              // console.log(e)
             }
    }
    render() {
        const {email,password,confrmPassword,firstName,lastName}=this.state
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                
                <Header title="Create Account" back={this.props.navigation}/>
                <ScrollView>
                <View style={{ margin: 20 }}>
                <Input
                        placeholder='First name'
                        label="First name*"
                        labelStyle={{backgroundColor:'#fff'}}
                        leftIcon={{ type: 'font-awesome', name: 'user-o', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={firstName}
                        onChangeText={(firstName)=>this.setState({firstName})}
                    />
                     <Input
                        placeholder='Last name'
                        label="Last name*"
                        labelStyle={{backgroundColor:'#fff'}}
                        leftIcon={{ type: 'font-awesome', name: 'user-o', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={lastName}
                        onChangeText={(lastName)=>this.setState({lastName})}
                    />
                    <Input
                        placeholder='mail@example.com'
                        label="Email*"
                        labelStyle={{backgroundColor:'#fff'}}
                        leftIcon={{ type: 'font-awesome', name: 'envelope-o', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={email}
                        onChangeText={(email)=>this.setState({email})}
                    />
                    <Input
                        placeholder='Min 8 Character'
                        label="Password*"
                        labelStyle={{backgroundColor:'#fff'}}
                        leftIcon={{ type: 'ionicon', name: 'lock-closed-outline', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={password}
                        onChangeText={(password)=>this.setState({password})}
                    />
                    {/*
                    <Input
                        placeholder='Repeat Password'
                        labelStyle={{backgroundColor:'#fff'}}
                        leftIcon={{ type: 'font-awesome', name: 'lock', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={confrmPassword}
                        onChangeText={(confrmPassword)=>this.setState({confrmPassword})}
                    />*/
    }
                     <Button
                        title="Sign up"
                        buttonStyle={{ backgroundColor: '#F95738' ,marginTop:20}}
                        onPress={()=>{                                
                              this.onRegister()
                    }}
                    />
                </View>
                <View style={styles.secondContainer}>
                    <Text style={styles.orText}>or</Text>
                <Button
                        title="Continue with Facebook"
                        buttonStyle={{backgroundColor:'#3278EA'}}
                        icon={
                            <Icon
                              name="facebook"
                              type="font-awesome"
                              size={15}
                              color="white"
                              style={styles.icon}
                            />
                          }
                    />
                     <Button
                        title="Continue with Google"
                        buttonStyle={{backgroundColor:'#3278EA',marginTop:10}}
                        icon={
                            <Icon
                              name="google"
                              type="font-awesome"
                              size={15}
                              style={styles.icon}
                              color="white"
                            />
                          }
                    />
                    <Text style={styles.paragraph}>
        The quick brown fox jumps over the{" "}
        <Text style={styles.highlight}>lazy dog</Text>
      </Text>
      
                </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '95%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    secondContainer:{flex:1,justifyContent:'flex-start',marginHorizontal:20},
    orText:{color:"#9A9A9A",textAlign:'center',margin:10},
    icon:{marginRight:10},
    inputContainerStyle:{borderBottomWidth:0},
    paragraph: {margin: 24,fontSize: 14,textAlign: "center",color: "gray"},
    highlight: {color: "#F95738"},
});
export default signupScreen