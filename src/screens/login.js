import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity } from "react-native";
import { Button, Icon, Input } from 'react-native-elements'
import Header from '../components/header'
import {login} from '../backend/apis'
import AsyncStorage from '@react-native-community/async-storage';
const { width, height } = Dimensions.get('screen')
class loginScreen extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            email:'luiz.filipe.pinto@gmail.com',
            password:'Test',
            // email:'aziijnab@gmail.com',
            // password:'1122',
            loader:false
        }
    }
    onLogin=()=>{
        let payLoad={
            email: this.state.email,
            password: this.state.password
        }
        this.setState({loader:true})
        try{
            login(payLoad).then(data=>{
              //  
          if(typeof data =='object')
          {
               //alert(data.id)
              //alert('Successfully login ')
           
              this.setState({loader:false},()=>{
                AsyncStorage.setItem('@userId',data.id.id)
                AsyncStorage.setItem('@token',data.token)
                console.log(data)
               this.props.navigation.navigate('home',{userId:data.id.id,token:data.token})
              })  
          }
          else
          {
              alert(data)

          }
            })
             }catch(e)
             {
                 alert('=')
              // console.log(e)
             }
             this.setState({loader:false})
    }
    render() {
        const {email,password,loader}=this.state
        return (
            <View style={styles.container}>
                
                <Header title="Sign in" back={this.props.navigation}/>
                <View style={{ margin: 20 }}>
                    <Input
                        placeholder='Email'
                        leftIcon={{ type: 'font-awesome', name: 'envelope-o', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={email}
                        onChangeText={(email)=>this.setState({email})}
                    />
                    <Input
                        placeholder='Paasword'
                        leftIcon={{ type: 'font-awesome', name: 'lock', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={password}
                        onChangeText={(password)=>this.setState({password})}
                    />
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('forgotPassword')}}>
                    <Text style={styles.forgotText}>Forgot Password</Text>
                    </TouchableOpacity>
                   
                     <Button
                        title="Sign in"
                        buttonStyle={{ backgroundColor: '#F95738' }}
                        loading={loader}
                        onPress={()=>{
                            this.onLogin()
                           // this.props.navigation.navigate('home')
                    }}
                    />
                </View>
                <View style={styles.secondContainer}>
                    <Text style={styles.orText}>or</Text>
                <Button
                        title="Continue with Facebook"
                        buttonStyle={{backgroundColor:'#3278EA'}}
                        icon={
                            <Icon
                              name="facebook"
                              type="font-awesome"
                              size={15}
                              color="white"
                              style={styles.icon}
                            />
                          }
                    />
                     <Button
                        title="Continue with Google"
                        buttonStyle={{backgroundColor:'#3278EA',marginTop:10}}
                        icon={
                            <Icon
                              name="google"
                              type="font-awesome"
                              size={15}
                              style={styles.icon}
                              color="white"
                            />
                          }
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '95%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    forgotText: { color: '#0D3B66', marginVertical: 20, marginLeft: 10 },
    secondContainer:{flex:1,justifyContent:'center',margin:20,marginTop:80},
    orText:{color:"#9A9A9A",textAlign:'center',margin:10},
    icon:{marginRight:10},
    inputContainerStyle:{borderBottomWidth:0}
});
export default loginScreen