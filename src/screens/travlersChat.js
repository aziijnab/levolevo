import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity, ScrollView } from "react-native";
import { Icon, ListItem, Avatar ,Badge} from 'react-native-elements'
import { Picker } from '@react-native-community/picker';
const { width, height } = Dimensions.get('screen')
const list = [
    {
      name: 'Amy Farhsaasa',
      avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
      subtitle: 'Vice President',
      batch:5
    },
    {
      name: 'Chris Jackson',
      avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
      subtitle: 'Vice Chairman',
      batch:4
    },
    
  ]
class travlerChat extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: 'active',
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <ScrollView style={{ margin: 10 }}>
                    <View style={styles.picker}>
                        <Picker
                            selectedValue={this.state.days}
                            mode={'dropdown'}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ days: itemValue })
                            }>
                            <Picker.Item label="active" value="1" />
                            <Picker.Item label="sort by origin" value="2" />
                            <Picker.Item label="sort by destination city" value="2" />
                        </Picker>
                    </View>
                    {
    list.map((l, i) => (
      <ListItem key={i} bottomDivider>
        <Avatar source={require('../assets/ui.png')} rounded  size={'large'}/>
        <ListItem.Content>
          <ListItem.Title>{l.name}</ListItem.Title>
          <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
        </ListItem.Content>
        <View>
        <ListItem.Subtitle>20:30</ListItem.Subtitle> 
        <Badge value={l.batch} status="error" />
        </View>
      </ListItem>
    ))
  }
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    picker:{
        borderColor: 'silver', borderWidth: 1, marginTop: 10, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        elevation: 5,
    }
  
});
export default travlerChat