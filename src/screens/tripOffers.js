import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import {  Icon, ListItem, Avatar, Button, Rating ,Input} from 'react-native-elements'
import Header from '../components/header'
const { width, height } = Dimensions.get('screen')
class tripOffers extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            mainArray:[1,2],
            subArry:[require('../assets/p1.png'),require('../assets/p2.png')]
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header title="Offer" back={this.props.navigation}/>
                <ScrollView>
              
               <View >
                    <View style={styles.tripHeader}>
                        <View>
                            <View style={styles.row}>
                                <Text style={styles.heading}>Manous</Text>
                                <Icon type="material" name="arrow-right-alt" />
                                <Text style={styles.heading}>Guarulhs</Text>
                            </View>
                            <Text style={styles.date}>25/01/2021</Text>
                        </View>
                       
                    </View>
         
                        <View>
                        <ListItem>
                            <Avatar source={require('../assets/p1.png')} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                            <ListItem.Content>
                                <ListItem.Title>Carlos Santana</ListItem.Title>
                                <Rating
                                    imageSize={15}
                                    style={{ paddingVertical: 10 }}
                                />
                                <ListItem.Subtitle>Documnet</ListItem.Subtitle>
                            
                            </ListItem.Content>
                        </ListItem>
                    </View>
                  </View>
                  <ListItem>
             
                            <ListItem.Content>
                                <ListItem.Title>Details:</ListItem.Title>
                            </ListItem.Content>
                        </ListItem>
                        <Input
                        placeholder='Short description'
                        label="Short description"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ borderWidth:1,borderColor:'#DADADA'}}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value="Document"
                    />
                    <Input
                        placeholder='WxHxD'
                        label="WxHxD"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ borderWidth:1,borderColor:'#DADADA'}}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value="30*30*30"
                    />
                     <Input
                        placeholder='Weight (kg)'
                        label="Weight (kg)"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ borderWidth:1,borderColor:'#DADADA'}}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value="2"
                    />
                      <Input
                        placeholder='Date to send'
                        label="Date to send"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ borderWidth:1,borderColor:'#DADADA'}}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value="25/01/2021"
                    />
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    b1: { width: width / 3, height: 30, justifyContent: 'center', marginTop: 10 },
    input: { backgroundColor: '#fff', width: '90%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle: { borderBottomWidth: 0 },

});
export default tripOffers