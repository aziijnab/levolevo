import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating } from 'react-native-elements'

const { width, height } = Dimensions.get('screen')
class reviewOffers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mainArray: [1, 2],
            subArry: [require('../assets/p1.png'), require('../assets/p2.png')]
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header
                    // rightComponent={{ icon: 'plus', type: 'font-awesome', color: '#F95738', onPress: () => { this.props.back.goBack() } }}
                    centerComponent={{ text: 'Offers', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />
                <ScrollView>

                    <View >
                        <View style={styles.tripHeader}>
                            <View>
                                <View style={styles.row}>
                                    <Text style={styles.heading}>Manous</Text>
                                    <Icon type="material" name="arrow-right-alt" />
                                    <Text style={styles.heading}>Guarulhs</Text>
                                </View>
                                <Text style={styles.date}>25/01/2021</Text>
                            </View>

                        </View>
                        <Text style={styles.matchesContainer}>2 matches</Text>
                        {this.state.subArry.map((item, index) => (
                            <View>
                                <ListItem>
                                    <Avatar source={item} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                                    <ListItem.Content>
                                        <ListItem.Title>Carlos Santana</ListItem.Title>
                                        <Rating

                                            imageSize={15}
                                            style={{ paddingVertical: 10 }}
                                        />
                                        <View style={styles.row}>
                                            <Text>Your bid</Text>
                                            <Text style={{ marginLeft: 10, backgroundColor: '#F4F6FB', padding: 10 }}>$39 </Text>
                                        </View>
                                    </ListItem.Content>
                                </ListItem>
                                <Button
  title="Hired"
buttonStyle={[styles.b1,{backgroundColor:"#1F8916"}]}
onPress={()=>{this.props.newProps.navigate('viewOffers')}}
/>
                            </View>
                        ))}
                    </View>


                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    editIcon: { margin: 10 },
    matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
    b1: { width: (width -50), height: 47, justifyContent: 'center', marginTop: 10,alignSelf:'center' }

});
export default reviewOffers