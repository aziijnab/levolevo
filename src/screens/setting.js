import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity } from "react-native";
import {   Icon, ListItem, Avatar  } from 'react-native-elements'
import { ScrollView } from "react-native-gesture-handler";
import Header from '../components/header'

const { width, height } = Dimensions.get('screen')
class setting extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                
                <Header title="Settings" back={this.props.navigation}/>
                <ScrollView>
          
               <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('profile')}}>
        <Avatar source={{uri: 'https://i.pinimg.com/originals/ff/a0/9a/ffa09aec412db3f54deadf1b3781de2a.png'}}
        size={'medium'} />
        <ListItem.Content>
          <ListItem.Title>Jhon Jane</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron size={25}/>
      </ListItem>
      <ListItem bottomDivider>
      <Icon name="bell" type="font-awesome" size={25} color="#fff" style={{backgroundColor:'#FFE583',padding:10}}/>
        <ListItem.Content>
          <ListItem.Title>Notifications</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron size={25}/>
      </ListItem>
      <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('payments')}}>
      <Icon name="payment" type="material" size={25} color="#fff" style={{backgroundColor:'#F4D35E',padding:10}}/>
        <ListItem.Content>
          <ListItem.Title>Payment History</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron size={25}/>
      </ListItem>
      <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('wallet')}}>
      <Icon name="wallet" type="font-awesome-5" size={25} color="#fff" style={{backgroundColor:'#EE964B',padding:10}}/>
        <ListItem.Content>
          <ListItem.Title>Wallet</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron size={25}/>
      </ListItem>
      <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('inviteFriends')}}>
      <Icon name="share-alt" type="font-awesome" size={25} color="#fff" style={{backgroundColor:'#EF6146',padding:10}}/>
        <ListItem.Content>
          <ListItem.Title>Invite Friends</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron size={25}/>
      </ListItem>
      <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('terms')}}>
      <Icon name="text-document" type="entypo" size={25} color="#fff" style={{backgroundColor:'#E63946',padding:10}}/>
        <ListItem.Content>
          <ListItem.Title>Terms of Use</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron size={25}/>
      </ListItem>
                
      </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },   
});
export default setting