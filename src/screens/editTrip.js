import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Button, Image, Input } from 'react-native-elements'
import Header from '../components/header'

const { width, height } = Dimensions.get('screen')

import { UpdateTrip, getAirports, DeleteTrip, updateTripViews } from '../backend/apis'
import DropDownPicker from 'react-native-dropdown-picker';

import AsyncStorage from '@react-native-community/async-storage';
import DateTimePickerModal from "react-native-modal-datetime-picker";

class editTrip extends Component {
    constructor(props) {
        super(props)
        this.state = {

            origin: null,
            dest: '',
            airports: [],
            token: '',
            userId: '',
            date: new Date(),
            showDate: false,
            showTime: false,
            time: new Date(),
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            selectedOrigin: this.props.route.params?.tripData.trip.origin ?? 'org',
            selectedDest: this.props.route.params?.tripData.trip.dest ?? 'des',
            tripId: this.props.route.params?.tripData.trip._id,
            status: 'active',
            views:0
        }
        console.log(this.state.tripId)
    }
    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true })
    };
    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false });
    };
    handleConfirmDatePicker = (date) => {
        let x = date
        var date = x.toISOString().slice(0, 10);
        this.setState({
            date: date
        })
        this.hideDatePicker();
    };

    showTimePicker = () => {
        this.setState({ isTimePickerVisible: true })
    };
    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };
    handleConfirmTimePicker = (date) => {
        let x = date.toTimeString().substr(0, 5)
        this.setState({ time: x })
        this.hideTimePicker();
    };
    async componentDidMount() {


        this.setState({
            date: this.props.route.params?.tripData.trip.date ?? new Date(),
            time: this.props.route.params?.tripData.trip.time ?? new Date(),
        })
        const token = await AsyncStorage.getItem('@token');
        if (token != null) { this.setState({ token: token }) }
        else { }
        const userId = await AsyncStorage.getItem('@userId');
        if (userId != null) { this.setState({ userId: userId }) }
        else { }

        //get and save views
        try {
            let payLoad = {
                uId: this.state.userId,
                token: this.state.token,
                tripId: this.state.tripId
            }
    
            updateTripViews(payLoad).then(data => {
                //  
                if (typeof data == 'object') {
              
                    this.setState({
                        views: data.views
                    })
                }
                else {
                    alert(data+'p')
                }
            })
        } catch (e) {
            alert('=')
            // console.log(e)
        }
        try {

            getAirports().then(data => {
                //  
                if (typeof data == 'object') {
                    //alert(data.id)
                    let tempAirport = []
                    for (let i = 0; i < data.airports.length; i++) {
                        //    console.log(data.airports[i].IATA)
                        if (this.state.selectedOrigin == data.airports[i].IATA) {

                            this.setState({
                                origin: data.airports[i].City
                            })
                        }
                        if (this.state.selectedDest == data.airports[i].IATA) {

                            this.setState({
                                dest: data.airports[i].City
                            })
                        }
                        tempAirport.push({
                            label: data.airports[i].City,
                            value: data.airports[i].City,
                            IATA: data.airports[i].IATA,
                        })
                    }
                    this.setState({
                        airports: tempAirport
                    })
                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            alert('=')
            // console.log(e)
        }
    }

    UpdateTrip = () => {
        var tripData = {
            origin: this.state.selectedOrigin,
            dest: this.state.selectedDest,
            date: this.state.date,
            time: this.state.time,
            status: this.state.status
        }
        let payLoad = {
            uId: this.state.userId,
            token: this.state.token,
            tripId: this.state.tripId
        }


        try {
            UpdateTrip(tripData, payLoad).then(data => {
                //  
                if (typeof data == 'object') {
                    //alert(data.id)
                    //  alert('Successfully login ')
                    console.log(data)
                    this.props.navigation.navigate('myTrips', { value: 'x' })
                    // alert('trip Updated')
                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            alert('=')
        }

    }

    deleteTrip = () => {

        let payLoad = {
            uId: this.state.userId,
            token: this.state.token,
            tripId: this.state.tripId
        }
        try {
            DeleteTrip(payLoad).then(data => {
                console.log(data)
                this.props.navigation.navigate('myTrips', { value: 'x' })

            })
        } catch (e) {
            alert('=')
        }
    }
    render() {
        var { origin, dest, date, dimension, date, showDate, showTime, time, selectedOrigin ,views} = this.state
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header title="Trips" back={this.props.navigation} />
                <Text style={styles.secondLabel}>Views ({views})</Text>
                <ScrollView>
                    <Text style={styles.secondLabel}>From</Text>
                    <DropDownPicker
                        items={this.state.airports}

                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        placeholder={origin}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {
                            this.setState({
                                origin: item.IATA,
                                selectedOrigin: item.IATA
                            }, () => {
                            })
                        }}
                        searchable
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                    <Text style={styles.secondLabel}>To</Text>
                    <DropDownPicker
                        items={this.state.airports}
                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        placeholder={dest}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {
                            this.setState({
                                dest: item.IATA,
                                selectedDest: item.IATA
                            })
                        }}
                        searchable
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                    <Input
                        placeholder='Date'
                        label="Date"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'calendar', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={date}
                        onFocus={this.showDatePicker}
                    />
                    <Input
                        placeholder='Time'
                        label="Time"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'clock-o', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={time}
                        onFocus={this.showTimePicker}
                    />
                    <Text style={styles.secondLabel}>Status</Text>
                    <DropDownPicker
                        items={[
                            { label: 'active', value: 'active' },
                            { label: 'In active', value: 'In active' },
                        ]}
                        defaultValue={this.state.status}
                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}

                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {

                        }}
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                    {
                        //date picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisible}
                        mode="date"
                        onConfirm={this.handleConfirmDatePicker}
                        onCancel={this.hideDatePicker}
                    />
                    {
                        //time picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isTimePickerVisible}
                        mode="time"
                        onConfirm={this.handleConfirmTimePicker}
                        onCancel={this.hideTimePicker}
                    />

                    <Button
                        title="Update"
                        buttonStyle={{ backgroundColor: '#F95738', margin: 20 }}
                        onPress={this.UpdateTrip}
                    />
                    <Button title="Delete Trip" type="outline" buttonStyle={{ margin: 10 }}
                        onPress={this.deleteTrip}
                    />
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '90%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle: { borderBottomWidth: 0 },
    labelStyle: { backgroundColor: '#fff', color: '#000', fontWeight: '300' },
    pcikerLabel: { marginLeft: 25, fontSize: 15, marginTop: 16 },
    uploadButton: { position: 'absolute', bottom: 10, backgroundColor: 'rgba(0,0,0,.5)', padding: 5, borderRadius: 10 },
    secondLabel: { fontSize: 18, marginLeft: '6%', marginTop: 16, fontWeight: '300' }

});
export default editTrip