import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import { getTrips } from '../backend/apis'
const { width, height } = Dimensions.get('screen')
class myTrips extends Component {
    constructor(props) {
        super(props)
        this.state = {
            trips: [],
            subArry: [require('../assets/p1.png'), require('../assets/p2.png')]
        }
    }
    async componentDidMount() {
        const token = await AsyncStorage.getItem('@token');
        if (token != null) { this.setState({ token: token }) }
        else { }
        const userId = await AsyncStorage.getItem('@userId');
        if (userId != null) {
            this.setState({ userId: userId }, () => {
                try {
                    getTrips(this.state.userId, this.state.token).then(data => {
                        console.log("<><><><><><><><><><><")
                        console.log(data)
                        if (typeof data == 'object') {

                            if (data[0].trip != null) {
                        console.log(data)
                                this.setState({ trips: data })
                            }
                            else {
                                this.setState({
                                    trips: [],
                                })
                                alert('No trips found')
                            }
                        }
                        else {
                            alert(data)
                        }
                    })
                } catch (e) {
                    alert('=')
                }
            })
        }
        else { }


    }
    componentWillReceiveProps() {
        try {
            getTrips(this.state.userId, this.state.token).then(data => {
                //
                if (typeof data == 'object') {

                    if (data[0].trip != null) {
                        this.setState({ trips: data })
                    }
                    else {
                        this.setState({
                            trips: [],
                        })
                        alert('No trips found')
                    }

                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            alert('=')
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header
                    rightComponent={{ icon: 'plus', type: 'font-awesome', color: '#F95738', onPress: () => { this.props.navigation.navigate('addTrip') } }}
                    centerComponent={{ text: 'My trips', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />

                <ScrollView>
                    {this.state.trips.map((item, index) => {
                        let tripid=item.trip._id;
                        return(
                        <View key={index}>
                            <View style={styles.tripHeader}>
                                <View>
                                    <View style={styles.row}>
                                        <Text style={styles.heading}>{item.trip.origin}</Text>
                                        <Icon type="material" name="arrow-right-alt" />
                                        <Text style={styles.heading}>{item.trip.dest}</Text>
                                    </View>
                                    <Text style={styles.date}>{item.trip.date}</Text>
                                </View>
                                <View>
                                    <TouchableOpacity onPress={() => {
                                        this.props.navigation.navigate('editTrip', { tripData: item })
                                    }}>
                                        <Icon type="font-awesome" name="pencil" size={15} style={styles.editIcon} />
                                    </TouchableOpacity>

                                </View>
                            </View>
                            <Text style={styles.matchesContainer}>{item.matches} matches</Text>
                          {item.trip.matchingOrders!=0?
                          <>
                              {  item.trip.matchingOrders.map((item, index) => (
                                <View>
                                    <ListItem>
                                        <Avatar source={{uri:item.image}} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                                        <ListItem.Content>
                                            <ListItem.Title>{item.senderName}</ListItem.Title>
                                            <Rating
                                            startingValue={item.senderStarRank}
                                            imageSize={15}
                                            ratingCount={5}
                                            style={{ paddingVertical: 10 }}
                                        />

                                            <ListItem.Subtitle>{item.description}</ListItem.Subtitle>

                                            <Button
                                                title="Details"
                                                buttonStyle={{ backgroundColor: "#F95738" }}
                                                containerStyle={styles.b1}
                                                onPress={() => {

                                                   this.props.navigation.navigate('reviewOrderDetails', { orderDetails: item })
                                                }}
                                            />

                                        </ListItem.Content>
                                    </ListItem>
                                </View>
                            ))}
                          </>:
                          <></>
    }

                        </View>
                    )})}


                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    editIcon: { margin: 10 },
    matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
    b1: { width: width / 3, height: 30, justifyContent: 'center', marginTop: 10 }

});
export default myTrips
