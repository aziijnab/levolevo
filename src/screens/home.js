import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating } from 'react-native-elements'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import MyOrders from '../screens/myOrders';
import ViewOrffers from '../screens/viewOffers';
const { width, height } = Dimensions.get('screen')



const LazyPlaceholder = ({ route }) => (
    <View style={styles.scene}>
      <Text>Loading {route.title}…</Text>
    </View>
  );
class home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            index: 0,
            routes: [
                { key: 'orders', title: 'My Orders' },
                { key: 'travels', title: 'My Travels' },
                { key: 'offers', title: 'My Offers' },
            ],
        }
       
    }
   
    _handleIndexChange = index => this.setState({ index });
    _renderLazyPlaceholder = ({ route }) => <LazyPlaceholder route={route} />;
    _renderLabel = (scene,focused) => {
        const label = scene.route.title
        return (
            <Text style={{ color: scene.focused?'#F95738':'#9599A1', fontWeight: 'bold' }}>{label}</Text>
        );
    }
    render() {
       
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header
              
                    centerComponent={{ text: 'Home', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />
              
                    <TabView
                        lazy
                        swipeEnabled={true}
                        navigationState={this.state}
                        renderScene={SceneMap({
                            orders: ()=>{return(<MyOrders newProps={this.props.navigation} />)},
                            travels:()=> {return(<MyOrders newProps={this.props.navigation} />)},
                            offers:()=> {return(<MyOrders newProps={this.props.navigation} />)},
                          })}
                        renderLazyPlaceholder={this._renderLazyPlaceholder}
                        onIndexChange={this._handleIndexChange}
                        style={{
                            marginTop: 0,
                            backgroundColor: '#fff'
                        }}
                        renderTabBar={(props) =>
                            <TabBar
                                {...props}
                                style={{ backgroundColor: "#fff", padding: 1 }}
                                indicatorStyle={{ backgroundColor: "#F95738" }}
                                renderLabel={this._renderLabel}
                            />
                        }
                    />
          
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    editIcon: { margin: 10 },
    matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
    b1: { width: width / 3, height: 30, justifyContent: 'center', marginTop: 10 }

});
export default home