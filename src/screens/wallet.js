import React, { Component } from "react";
import { StyleSheet,  View, StatusBar, Image, Dimensions, ScrollView } from "react-native";
import {   Icon, ListItem, Avatar  ,Button} from 'react-native-elements'

import Header from '../components/header'

const { width, height } = Dimensions.get('screen')
class Wallet extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                
                <Header title="Wallet" back={this.props.navigation}/>
           
          
               
     
      <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('payments')}} containerStyle={{backgroundColor:"#FAF0CA"}}>
    
        <ListItem.Content>
          <ListItem.Title>Payout Method</ListItem.Title>
         
        </ListItem.Content>
        
      </ListItem>
      <ListItem>
      <ListItem.Content>
          <ListItem.Title>You don't have a payout method yet</ListItem.Title>
         <ListItem.Subtitle>Please set your payout metho to start receving money</ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
      <View style={styles.b1Wraper}>
      <Button
                        title="Set up payout"
                        buttonStyle={styles.b1}
                    />
      </View>
     
     
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },   
    b1:{ backgroundColor: '#F95738' ,width:'90%',alignSelf:'center'},
    b1Wraper:{position:'absolute',width:width,bottom:30}

});
export default Wallet