import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, ImageBackground } from "react-native";
import {  Icon, ListItem, Avatar, Divider } from 'react-native-elements'
import { ScrollView } from "react-native-gesture-handler";

import Header from '../components/header'
const { width, height } = Dimensions.get('screen')
class profile extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
               
                <Header title="My profile" back={this.props.navigation}/>
                <ScrollView>
                    <ImageBackground
                        source={{ uri: 'https://i.pinimg.com/originals/ff/a0/9a/ffa09aec412db3f54deadf1b3781de2a.png' }}
                        style={styles.profileImage}
                        resizeMode="contain"
                    >
                        <Text
                            style={styles.profileText}
                        >
                            Edit
                   </Text>
                    </ImageBackground>
                    <View style={styles.editRow}>
                        <Text style={styles.userName}>Jone Jane</Text>
                        <Icon name="pencil" type="font-awesome" size={18} style={{ margin: 15 }} />
                    </View>
                    <Text style={styles.date}>Joined on 17 November2021</Text>
                    <Divider style={{ backgroundColor: '#EEEEEE', marginVertical: 10 }} />
                    <ListItem onPress={() => { this.props.navigation.navigate('profile') }}>

                        <ListItem.Content>
                            <View style={styles.row}>
                                <ListItem.Subtitle>Phone number</ListItem.Subtitle>
                                <Icon name="check-circle" type="font-awesome" color="#1F8916" size={18} style={{ marginLeft: 15 }} />
                            </View>

                            <ListItem.Title>+41 111 1111 11</ListItem.Title>

                        </ListItem.Content>
                        <View style={styles.row}>
                            <Icon name="pencil" type="font-awesome" size={18} color="#0D3B66" style={{ marginRight: 5 }} />
                            <Text style={styles.editText}>Edit</Text>
                        </View>
                    </ListItem>
                    <ListItem containerStyle={{ backgroundColor: '#FAF0CA' }}>
                        <ListItem.Content>
                            <View style={styles.row}>
                                <ListItem.Title>ADDRESS</ListItem.Title>
                                <Icon name="check-circle" type="font-awesome" color="#1F8916" size={18} style={{ marginLeft: 15 }} />
                            </View>
                        </ListItem.Content>
                        <View style={styles.row}>
                            <Icon name="pencil" type="font-awesome" size={18} color="#0D3B66" style={{ marginRight: 5 }} />
                            <Text style={styles.editText}>Edit</Text>
                        </View>
                    </ListItem>
                    <ListItem>
                        <ListItem.Content>
                        <ListItem.Subtitle>Street name</ListItem.Subtitle>
                        <ListItem.Title>High Road Avenue</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                    <ListItem>
                        <ListItem.Content>
                        <ListItem.Subtitle>Number</ListItem.Subtitle>
                        <ListItem.Title>75</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                    <ListItem>
                        <ListItem.Content>
                        <ListItem.Subtitle>Postal Code</ListItem.Subtitle>
                        <ListItem.Title>31-9384</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                    <ListItem>
                        <ListItem.Content>
                        <ListItem.Subtitle>City</ListItem.Subtitle>
                        <ListItem.Title>Las Vegas</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    profileImage: { height: 150, width: 150, justifyContent: 'flex-end', alignItems: 'center', alignSelf: 'center', },
    profileText: { fontWeight: 'bold', color: 'white', backgroundColor: 'rgba(0,0,0,.5)', width: 100, textAlign: 'center', marginBottom: 2, padding: 5 },
    userName: { color: '#292929', fontSize: 18, fontWeight: '600' },
    editRow: { flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15 },
    date: { color: '#939393', textAlign: 'center', fontSize: 14 },
    row: { flexDirection: 'row' },
    editText: { color: '#0D3B66' }
});
export default profile