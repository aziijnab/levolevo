import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity } from "react-native";
import { Button, Icon, Input } from 'react-native-elements'
import Header from '../components/header'
import {ForgotPassword} from '../backend/apis'
const { width, height } = Dimensions.get('screen')
class forgotPassword extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            email:'zoraze5572615@gmail.com',
        }
    }
    onForgot=()=>
    {
        let payLoad={
            email: this.state.email    
        }
        try{
            ForgotPassword(payLoad).then(data=>{
          
           console.log(typeof data)
           if(typeof data =='object')
           {
                alert(data)
           }
           else
           {
               alert(data)
           }
            })
             }catch(e)
             {
                 alert('=')
             }
    }
    render() {
        const {email}=this.state
        return (
            <View style={styles.container}>
                
                <Header title="Forgot password" back={this.props.navigation}/>
                <View style={{ margin: 20 }}>
                    <Input
                        placeholder='Email'
                        leftIcon={{ type: 'font-awesome', name: 'envelope-o', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        value={email}
                        onChangeText={(email)=>this.setState({email})}
                    />
                   <Text style={styles.info}>
                   Provide your e-mail address to which we will send the verification link. After opening the link, it will be possible to set a new password.
                   </Text>
                    
                     <Button
                        title="Send"
                        buttonStyle={{ backgroundColor: '#F95738' ,marginVertical:20}}
                        onPress={()=>{
                            this.onForgot()
                            //this.props.navigation.navigate('resetPassword')
                        }}
                    />
                    <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                    <Text style={styles.return}>
     Return to sign in
                    </Text>
                    </TouchableOpacity>
                  
                </View>
          
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '95%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle:{borderBottomWidth:0},
    info:{color:'#9599A1',margin:10,fontSize:14,textAlign:'center'},
    return:{color:'#0D3B66',textAlign:'center',fontSize:14,textDecorationLine: "underline", textDecorationColor: "#0D3B66"}
   
});
export default forgotPassword