import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, TouchableOpacity ,ScrollView} from "react-native";
import {   Button, Icon  ,Header } from 'react-native-elements'

import Textarea from 'react-native-textarea'

const { width, height } = Dimensions.get('screen')
class RejectParcel extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                
                <Header
                   // rightComponent={{ icon: 'plus', type: 'font-awesome', color: '#F95738', onPress: () => { this.props.back.goBack() } }}
                    centerComponent={{ text: 'Rejecting the parcel', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />
                <ScrollView>
                <View style={styles.areaContainer}>
                    <Text>Reason of parsel rejecting</Text>
  <Textarea
    containerStyle={styles.textareaContainer}
    style={styles.textarea}
   // onChangeText={this.onChange}
    //defaultValue={this.state.text}
    maxLength={120}
    placeholder={'好玩有趣的，大家同乐，伤感忧闷的，大家同哭。。。'}
    placeholderTextColor={'#c7c7c7'}
    underlineColorAndroid={'transparent'}
  />
  <View style={styles.row}>
      <Icon type="font-awesome" name="upload" size={12}/>
<Text>Upload a picture of the parcel you are declining to transport</Text>
  </View>
</View>
<Button
  title="Save"
  buttonStyle={{backgroundColor:'#F95738'}}
  containerStyle={styles.b1}
 
/>
      </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    areaContainer: {
        flex: 1,
        padding: 30,
        justifyContent: 'center',
       
      },
    textareaContainer: {
        height: 180,
        padding: 5,
        backgroundColor: '#fff',
        borderColor:'#DADADA',borderWidth:1,
        width:width-40,alignSelf:'center'
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      },
    b1:{width:width-50,alignSelf:'center',marginTop:height/4} ,
    row:{flexDirection:'row',marginTop:30,alignItems:'center',justifyContent:'space-between'}
});
export default RejectParcel