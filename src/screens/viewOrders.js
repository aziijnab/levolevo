import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity, Image } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating } from 'react-native-elements'

const { width, height } = Dimensions.get('screen')
import AsyncStorage from '@react-native-community/async-storage';
import { getOrders } from '../backend/apis'
class viewOrder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      orders: []
    }
  }
  async componentDidMount() {
    const token = await AsyncStorage.getItem('@token');
    if (token != null) { this.setState({ token: token }) }
    else { }
    const userId = await AsyncStorage.getItem('@userId');
    if (userId != null) { this.setState({ userId: userId }) }
    else { }
    this.getOrders();
  }
  getOrders() {
    try {
      let payLoad = {
        uId: this.state.userId,
        token: this.state.token
      }
      console.log("payLoad -> Get Orrders")
      console.log(payLoad)
      getOrders(payLoad).then(data => {
        console.log(data,'---')
        if (typeof data == 'object') {
          console.log(data,'---')
          this.setState({
            orders: data
          })

        }
        else {
          alert(data)
        }
      })
    } catch (e) {
      alert('=')
      // console.log(e)
    }
  }

  componentWillReceiveProps() {
    this.getOrders();
  }
  render() {
    const { orders } = this.state
    console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    console.log(orders.length)
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />

        <ScrollView>
          {
            /*   orders.length>0?
               <Text style={{
                   alignSelf:'center',
                 marginTop:height/2
               }}>No oredrs</Text>
               :*/
            <>
              {
                orders.length > 0 ?
                orders.map((item, index) => {
                  console.log("item")
                  console.log(item.order)
if(item.order){
  return (
    <TouchableOpacity key={index} onPress={() => {
                      this.props.newProps.navigate('editOrder', { orderData: item })
                    }}>
                      <ListItem>
                        {
                        <Avatar source={{ uri:  item.order.prodImage }} imageProps={{ resizeMode: 'center' }} size={'xlarge'} />
                }
                        <ListItem.Content>
                          <View style={styles.row}>
                            <Text style={styles.heading}>{item.order.description}</Text>
                            {/* <Icon type="material" name="arrow-right-alt" />
                            <Text style={styles.heading}>{item.order.dest}</Text> */}
                          </View>
                          {/* <ListItem.Title>Carlos Santana</ListItem.Title> */}
                          <ListItem.Subtitle>Send:</ListItem.Subtitle>
                          <Text style={{ fontSize: 12, marginVertical:3, fontWeight:"bold" }}>{item.order.date} to {item.order.dateMax}</Text>
                          <ListItem.Subtitle style={{fontWeight:"bold"}}>Offers :{item.order.offers ? item.order.offers.length : 0}</ListItem.Subtitle>

                          <View style={{flexDirection:"row"}}>
                            <View style={{width:"80%"}}>
                            <Text>{item.order.date}</Text>
                            </View>
                            <View style={{width:"20%", flexDirection:"row"}}>
                            <Text >{item.order.views}</Text>
                            <Image style={{marginTop:3, marginLeft:2}} source={require('../assets/views.png')}/>
                            </View>

                            {/* <Icon type="material" name="arrow-right-alt" />
                            <Text style={styles.heading}>{item.order.dest}</Text> */}
                          </View>


                          {/* <Button
                          title="Add Offer"
                          buttonStyle={[styles.b1, { backgroundColor: '#0D3B66' }]}
                          onPress={() => {
                            this.props.newProps.navigate('addOffer',{orderId:item.order._id})
                          }}
                        /> */}
                        </ListItem.Content>
                      </ListItem>

                      <ListItem>
                        <Button
                          title="View offers"
                          buttonStyle={[styles.b1, { backgroundColor: "#F95738" }]}
                          onPress={() => {
                             this.props.newProps.navigate('viewOffers',{orederList:item.order.offers,orderId:item.order._id})
                             }}
                          disabled={item.order.offers ? item.order.offers.length > 0 ? false : true : true}
                        />
                        <Button
                          title="View travlers "
                          buttonStyle={[styles.b1, { backgroundColor: '#0D3B66' }]}
                          onPress={() => {
                             this.props.newProps.navigate('viewTravlers',{travelersList:item.order.matchingTravelers,orderId:item.order._id,matches:item.matches,origin:item.order.origin,dest:item.order.dest,date:item.order.date})
                             }}
                          // onPress={() => { this.props.newProps.navigate('viewTravlers') }}
                        />

                      </ListItem>
                    </TouchableOpacity>
                    )
}

                })
                :
                null
                }

            </>
          }


        </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  header: { backgroundColor: '#fff' },
  tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
  heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
  date: { color: '#878787', margin: 10 },
  editIcon: { margin: 10 },
  matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
  b1: { width: (width - 50) / 2, height: 30, justifyContent: 'center', marginTop: 10 },
  row: { flexDirection: 'row', justifyContent: 'space-evenly' }

});
export default viewOrder
