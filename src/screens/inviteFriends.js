import React, { Component } from "react";
import { StyleSheet, View, StatusBar, ActivityIndicator, Dimensions, ScrollView } from "react-native";
import { Icon, ListItem, Avatar, Button,Image } from 'react-native-elements'

import Header from '../components/header'

const { width, height } = Dimensions.get('screen')
class InviteFriends extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />

                <Header title="Invite Friends" back={this.props.navigation} />
<View style={styles.imageWraper}>
<Image
  source={require('../assets/object.png')}
  style={{ width: 278, height: 212 }}
  PlaceholderContent={<ActivityIndicator />}
/>
</View>
        
                <View style={styles.b1Wraper}>
                    <Button
                        title="Share a link"
                        buttonStyle={styles.b1}
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    b1: { backgroundColor: '#F95738', width: '90%', alignSelf: 'center' },
    b1Wraper: { position: 'absolute', width: width, bottom: 30 },
    imageWraper:{flex:1,justifyContent:'center',alignItems:'center'}

});
export default InviteFriends