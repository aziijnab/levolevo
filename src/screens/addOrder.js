import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Button, Image, Input } from 'react-native-elements'
import Header from '../components/header'
import { Picker } from '@react-native-community/picker';
const { width, height } = Dimensions.get('screen')
import ImagePicker from 'react-native-image-picker';
import { AddOrder, getAirports } from '../backend/apis'
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';
import DateTimePickerModal from "react-native-modal-datetime-picker";
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
var formdata = new FormData();
class addOrder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      days: '!1 days',
      image: '',
      origin: '',
      dest: '',
      date: '',
      dateMax: '',
      description: '',
      prodImage: '',
      weight: '',
      dimension: '',
      country: 'uk',
      airports: [],
      token: '',
      userId: '',
      isDatePickerVisible: false,
      isDatePickerVisiblemax:false
    }

  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem('@token');
    if (token != null) { this.setState({ token: token }) }
    else { }
    const userId = await AsyncStorage.getItem('@userId');
    if (userId != null) { this.setState({ userId: userId }) }
    else { }
    try {
      getAirports().then(data => {
        //  
        if (typeof data == 'object') {
          //alert(data.id)
          let tempAirport = []
          for (let i = 0; i < data.airports.length; i++) {
            tempAirport.push({
              label: data.airports[i].City,
              value: data.airports[i].City,
              IATA: data.airports[i].IATA,
            })
          }
          this.setState({
            airports: tempAirport
          })
        }
        else {
          alert(data)
        }
      })
    } catch (e) {
      alert('=')
      // console.log(e)
    }
  }
  showImagePicker = () => {
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {

      } else if (response.error) {

      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        //console.log('111');
        console.log(source);

        this.setState({ image: source.uri }, () => {
          this.uploadImage(response)
        })
        // 
      }
    });
  }
  addOrder = () => {
    formdata.append("origin", this.state.origin);
    formdata.append("dest", this.state.dest);
    formdata.append("date", this.state.date);
    formdata.append("dateMax", this.state.dateMax);
    formdata.append("description", this.state.description);
    formdata.append("weight", this.state.weight);
    formdata.append("dimension", this.state.dimension);
    let payLoad = {
      uId: this.state.userId,
      token: this.state.token
    }
    try {
      AddOrder(formdata, payLoad).then(data => {
        formdata=new FormData()
        if (typeof data == 'object') {
          
          this.props.navigation.navigate('orderHome',{ value: 'x' })
        }
        else {

          alert(data )
        }
      })
    } catch (e) {
      alert('=')
    }
  }
  async uploadImage(photo) {



    formdata.append("prodImage", {
      uri: photo.uri,
      type: 'image/jpeg', // or photo.type
      name: photo.fileName
    })



  }
  showDatePicker = () => {
    this.setState({ isDatePickerVisible: true })
  };
  hideDatePicker = () => {
    this.setState({ isDatePickerVisible: false });
  };
  handleConfirmDatePicker = (date) => {
    let x = date
    console.log(x, '___')
    var date = x.toISOString().slice(0, 10);
    this.setState({
      date: date
    })
    this.hideDatePicker();
  };

  showDatePickerm = () => {
    this.setState({ isDatePickerVisiblemax: true })
  };
  hideDatePickerm = () => {
    this.setState({ isDatePickerVisiblemax: false });
  };
  handleConfirmDatePickerm = (date) => {
    let x = date
    var date = x.toISOString().slice(0, 10);
    this.setState({
      dateMax: date
    })
    this.hideDatePickerm();
  };
  render() {
    var { image, origin, dest, date, dateMax, description, weight, dimension } = this.state
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Header title="Orders" back={this.props.navigation} />
        <View style={{ alignItems: 'center' }}>
          <Image
            source={image == "" ? require('../assets/p1.png') : { uri: image }}
            style={{ width: width, height: 200 }}
            resizeMode={image == "" ? "cover" : "center"}
          />
          {
            image == "" ?
              <TouchableOpacity style={styles.uploadButton} onPress={this.showImagePicker}>
                <Text style={{ color: '#fff' }}>Upload photo</Text>
              </TouchableOpacity> :
              <TouchableOpacity style={styles.uploadButton} onPress={this.showImagePicker}>
                <Text style={{ color: '#fff' }}>Change photo</Text>
              </TouchableOpacity>
          }
        </View>
        <ScrollView>
          <Text style={styles.secondLabel}>From</Text>
          <DropDownPicker
            items={this.state.airports}
            containerStyle={{ width: '90%', alignSelf: 'center' }}
            style={{ backgroundColor: '#F4F6FB' }}
            itemStyle={{ justifyContent: 'flex-start' }}
            placeholder={'From'}
            dropDownStyle={{ backgroundColor: '#fafafa' }}
            onChangeItem={item => {
             
              this.setState({
                origin: item.IATA
              })
            }}
            searchable
            dropDownMaxHeight={200}
            labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
          />
          <Text style={styles.secondLabel}>To</Text>
          <DropDownPicker
            items={this.state.airports}
            containerStyle={{ width: '90%', alignSelf: 'center' }}
            style={{ backgroundColor: '#F4F6FB' }}
            itemStyle={{ justifyContent: 'flex-start' }}
            placeholder={'To'}
            dropDownStyle={{ backgroundColor: '#fafafa' }}
            onChangeItem={item => {
              alert(item.IATA)
              this.setState({
                dest: item.IATA
              })
            }}
            searchable
            dropDownMaxHeight={200}
            labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
          />
          <Input
            placeholder='Short description'
            label="Short description"
            labelStyle={styles.labelStyle}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            value={description}
            onChangeText={(description) => { this.setState({ description }) }}
          />
            <Input
            placeholder='Weight'
            label="Weight"
            labelStyle={styles.labelStyle}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            value={weight}
            onChangeText={(weight) => { this.setState({ weight }) }}
            keyboardType='number-pad'
          />
          <Input
            placeholder='WxHxD'
            label="WxHxD"
            labelStyle={styles.labelStyle}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            value={dimension}
            onChangeText={(dimension) => { this.setState({ dimension }) }}
          />
         
          <Input
            placeholder='Date'
            label="Date to send"
            labelStyle={{ backgroundColor: '#fff' }}
            inputStyle={{ color: '#0D3B66' }}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            rightIcon={{ type: 'font-awesome', name: 'calendar', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
            value={date}
            onFocus={this.showDatePicker}
          />
          {
            //date picker
          }
          <DateTimePickerModal
            isVisible={this.state.isDatePickerVisible}
            mode="date"
            onConfirm={this.handleConfirmDatePicker}
            onCancel={this.hideDatePicker}
          />

            <Input
            placeholder='Date'
            label="Date Max"
            labelStyle={{ backgroundColor: '#fff' }}
            inputStyle={{ color: '#0D3B66' }}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            rightIcon={{ type: 'font-awesome', name: 'calendar', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
            value={dateMax}
            onFocus={this.showDatePickerm}
          />
          {
            //date picker
          }
          <DateTimePickerModal
            isVisible={this.state.isDatePickerVisiblemax}
            mode="date"
            onConfirm={this.handleConfirmDatePickerm}
            onCancel={this.hideDatePickerm}
          />


          <Button
            title="Save"
            buttonStyle={{ backgroundColor: '#F95738', margin: 20 }}
            onPress={this.addOrder}
          />
        </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  header: { backgroundColor: '#fff' },
  input: { backgroundColor: '#F4F6FB', width: '90%', alignSelf: 'center', marginTop: 16 },
  errorStyle: { margin: 0, padding: 0, height: 0 },
  inputContainerStyle: { borderBottomWidth: 0 },
  labelStyle: { backgroundColor: '#fff', color: '#000', fontWeight: '300' },
  pcikerLabel: { marginLeft: 25, fontSize: 15, marginTop: 16 },
  uploadButton: { position: 'absolute', bottom: 10, backgroundColor: 'rgba(0,0,0,.5)', padding: 5, borderRadius: 10 },
  secondLabel: { fontSize: 18, marginLeft: '6%', marginTop: 16, fontWeight: '300' }

});
export default addOrder