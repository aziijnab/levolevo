import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity } from "react-native";
import { Button, Icon, Input } from 'react-native-elements'

import Header from '../components/header'
const { width, height } = Dimensions.get('screen')
class resetPassword extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
               
                <Header title="Resetting password" back={this.props.navigation}/>
                <View style={{ margin: 20 }}>
                    <Input
                    label="NEW PASSWORD"
                    labelStyle={{backgroundColor:'#fff'}}
                        placeholder='Min 8 character'
                        leftIcon={{ type: 'font-awesome', name: 'lock', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                    />
                    <Input
                        placeholder='Repeat password'
                        leftIcon={{ type: 'font-awesome', name: 'lock', size: 20, color: '#000', style: { marginRight: 10 } }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                    />
                   
                    
                     <Button
                        title="Change password"
                        buttonStyle={{ backgroundColor: '#F95738' ,marginVertical:20}}
                    />
                   
                </View>
          
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '95%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle:{borderBottomWidth:0},
});
export default resetPassword