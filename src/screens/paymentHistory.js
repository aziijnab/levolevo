import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, ScrollView } from "react-native";
import {  Icon, ListItem, Avatar } from 'react-native-elements'

import Header from '../components/header'
const { width, height } = Dimensions.get('screen')
class paymentHistory extends Component {
    render() {
        return (
            <View style={styles.container}>
            
                <Header title="Payment History" back={this.props.navigation}/>
                <ScrollView>
                 <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('paymentRecevied')}}>
                     <ListItem.Content>
                         <ListItem.Title>Payments received</ListItem.Title>
                     </ListItem.Content>
                     <ListItem.Chevron size={25}/>
                 </ListItem>
                 <ListItem bottomDivider onPress={()=>{this.props.navigation.navigate('paymentSent')}}>
                     <ListItem.Content>
                         <ListItem.Title>Payments sent</ListItem.Title>
                     </ListItem.Content>
                     <ListItem.Chevron size={25}/>
                 </ListItem>

                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
});
export default paymentHistory