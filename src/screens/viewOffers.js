import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating } from 'react-native-elements'
import {acceptOffer,rejectOffer} from '../backend/apis'
import AsyncStorage from '@react-native-community/async-storage'
const { width, height } = Dimensions.get('screen')
class viewOffers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            offersList: this.props.route.params?.orederList ?? '',
            orderId: this.props.route.params?.orderId ?? '',
            token: '',
            userId: ''
        }

    }
    acceptOffer = (offerId) => {
        let payLoad = {
            uId: this.state.userId,
            token: this.state.token,
            orderId: this.state.orderId,
            offerId: offerId
        }
        try {
            acceptOffer( payLoad).then(data => {
              console.log("Accept Ofer response");
              console.log(data);
                if (typeof data == 'object') {
                    //alert(data.id)
                    //  alert('Successfully login ')
                    //this.props.navigation.navigate('myTrips',{value:'x'})
                    alert('offer accepted')

                }
                else {
                    console.log(data)
                }
            })
        } catch (e) {
            alert('=')
        }
    }
    rejectOffer = (offerId) => {

        let payLoad = {
            uId: this.state.userId,
            token: this.state.token,
            orderId: this.state.orderId,
            offerId: offerId
        }
        try {
            rejectOffer( payLoad).then(data => {
                //
                if (typeof data == 'object') {
                    //alert(data.id)
                    //  alert('Successfully login ')
                    //this.props.navigation.navigate('myTrips',{value:'x'})
                    alert('offer rejectetd')

                }
                else {
                    console.log(data)
                }
            })
        } catch (e) {
            alert('=')
        }
    }
    async componentDidMount() {
        const token = await AsyncStorage.getItem('@token');
        if (token != null) { this.setState({ token: token }) }
        else { }
        const userId = await AsyncStorage.getItem('@userId');
        if (userId != null) { this.setState({ userId: userId }) }
        else { }

    }
    render() {
        const { offersList } = this.state
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header

                    centerComponent={{ text: 'View Offers', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                />
                <ScrollView>


                    <View>
                        {
                            /*
                        <ListItem>
                            <Avatar source={require('../assets/ui.png')} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                            <ListItem.Content>
                                <ListItem.Title>Carlos Santana</ListItem.Title>

                                <Rating

                                    imageSize={15}
                                    style={{ paddingVertical: 10 }}
                                />
                                <Text style={{ fontSize: 12 }}>25/01/2221 to 28/01/2021</Text>
                                <ListItem.Subtitle>Offers :1</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>
                        */
                        }
                        {offersList.map((item, index) => (
                            <View key={index} style={styles.box}>
                                <ListItem>
                                    <Avatar source={{uri:item.travelerProfileImage}} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                                    <ListItem.Content>
                                        <ListItem.Title>{item.travelerName}</ListItem.Title>
                                        <Rating
                                        startingValue={item.travelerStarRank}
                                        imageSize={15}
                                        ratingCount={5}
                                        style={{ paddingVertical: 10 }}
                                    />


                                    <View>
                                        <View style={styles.row}>
                                            <Text style={styles.heading}>{item.pickUpPlace}</Text>
                                            <Icon type="material" name="arrow-right-alt" />
                                            <Text style={styles.heading}>{item.dest}</Text>
                                        </View>
                                        <Text style={styles.date}>{item.pickUpDate}</Text>
                                    </View>

                                    </ListItem.Content>
                                </ListItem>
                                <ListItem>

                                    <ListItem.Content >
                                        <ListItem.Title>
                                            Where to deliver the parcel
  </ListItem.Title>
                                        <ListItem.Title>
                                            Place : {item.pickUpPlace}
                                        </ListItem.Title>
                                        <ListItem.Title>
                                            Date : {item.pickUpDate}
                                        </ListItem.Title>
                                        <ListItem.Title>
                                            Time : {item.pickUpTime}
                                        </ListItem.Title>


                                    </ListItem.Content>
                                </ListItem>
                                <ListItem containerStyle={styles.totalWraper}>
                                    <ListItem.Content style={styles.row}>

                                        <ListItem.Title style={{ alignSelf: 'center', marginRight: 5 }}>
                                            Total
    </ListItem.Title>
                                        <ListItem.Title style={{ alignSelf: 'center', color: '#0D3B66' }}>
                                            ${item.totalPrice}
                                        </ListItem.Title>
                                    </ListItem.Content>
                                </ListItem>
                                <Text style={styles.feesText}>*Fees included</Text>
                                <ListItem>
                                    <Button
                                        title="Reject"
                                        type="outline"
                                        buttonStyle={[styles.b1]}
                                        onPress={() => {this.rejectOffer(item._id)
                                         //    this.props.navigation.navigate('rejectParcel')
                                             }}
                                    />

                                    <Button
                                        title="Accept"
                                        buttonStyle={[styles.b1, { backgroundColor: '#0D3B66' }]}
                                        onPress={()=>{

                                            this.acceptOffer(item._id)
                                        }}
                                    />
                                </ListItem>

                            </View>
                        ))}


                    </View>




                    {
                        /*

                                            <Text onPress={() => { this.props.navigation.navigate('receivedOrder') }}>Share sample button</Text>
                                            */
                    }
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    editIcon: { margin: 10 },
    matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
    b1: { width: (width - 70) / 2, height: 30, justifyContent: 'center', marginTop: 10 },
    row: { flexDirection: 'row' },
    totalWraper: { borderWidth: 1, borderColor: '#E5E5E5', width: '90%', alignSelf: 'center', alignItems: 'center' },
    feesText: { color: '#9599A1', textAlign: 'center', fontSize: 16 },
    box: { width: '95%', borderColor: 'silver', borderWidth: 1, alignSelf: 'center', elevation: 0, margin: 10 }

});
export default viewOffers
