import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Header, Icon, ListItem, Avatar, Button, Rating, Input } from 'react-native-elements'
import DateTimePickerModal from "react-native-modal-datetime-picker";
const { width, height } = Dimensions.get('screen')
class reviewOrderDetails extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            orderDetails: this.props.route.params?.orderDetails ?? '',
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            date: new Date(),
            showTime: false,
            time: new Date(),
            
        }
        console.log("this.props.route.params")
        console.log(this.props.route.params)
    }
    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true })
      };
      hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false });
      };
      handleConfirmDatePicker = (date) => {
        let x = date
        console.log(x, '___')
        var date = x.toISOString().slice(0, 10);
        this.setState({
          date: date
        })
        this.hideDatePicker();
      };


      showTimePicker = () => {
        this.setState({ isTimePickerVisible: true })
    };
    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };
    handleConfirmTimePicker = (date) => {
        let x = date.toTimeString().substr(0, 5)
        this.setState({ time: x })
        this.hideTimePicker();
    };
    render() {
        var { time, date} = this.state
        let order = this.state.orderDetails;
        console.log("order.image")
        console.log(order.image)
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header
                   // rightComponent={{ icon: 'plus', type: 'font-awesome', color: '#F95738', onPress: () => { this.props.back.goBack() } }}
                    centerComponent={{ text: 'Offer', style: { fontSize: 20, fontWeight: '600', color: '#0D3B66' } }}
                    containerStyle={styles.header}
                    back={this.props.navigation}
                />
                <ScrollView>

               <View style={{paddingBottom:50}}>
                    <View style={styles.tripHeader}>
                        <View>
                            <View style={styles.row}>
                                <Text style={styles.heading}>From</Text>
                                <Icon type="material" name="arrow-right-alt" />
                                <Text style={styles.heading}>To</Text>
                            </View>
                            <Text style={styles.date}>Date</Text>
                        </View>

                    </View>
                    <View>
                        <ListItem>
                            <Avatar source={{uri:order.image}} imageProps={{ resizeMode: 'cover' }} size={'xlarge'} />
                            <ListItem.Content>
                                <ListItem.Title>{order.senderName}</ListItem.Title>
                                <Rating
                                    imageSize={15}
                                    startingValue={order.senderStarRank}
                                    ratingCount={5}
                                    style={{ paddingVertical: 10 }}
                                />
                                <View style={styles.row}>
<Text>Your bid</Text>
                                </View>
                              

                            </ListItem.Content>
                        </ListItem>
                    </View>
                    <ListItem>
             
             <ListItem.Content>
                 <ListItem.Title style={{fontWeight:"bold"}}>Details:</ListItem.Title>
             </ListItem.Content>
         </ListItem>
                    <View style={{width:"100%", alignContent:"center", alignItems:"center"}}>
                    <View style={{width:"90%"}}>
                   <View style={{width:"100%", marginTop:5}}>
                       <Text style={{fontWeight:"bold"}}>Short Description</Text>
                       <View style={{width:"100%", marginTop:5, borderColor:"#efefef", borderWidth:1,borderRadius:5, padding:8}}>
                       <Text>Short Description</Text>
                       </View>
                   </View>
                   <View style={{width:"100%", marginTop:5}}>
                       <Text style={{fontWeight:"bold"}}>WxHxD (cm)</Text>
                       <View style={{width:"100%", marginTop:5, borderColor:"#efefef", borderWidth:1,borderRadius:5, padding:8}}>
                       <Text>30*30*30</Text>
                       </View>
                   </View>
                   <View style={{width:"100%", marginTop:5}}>
                       <Text style={{fontWeight:"bold"}}>Weight (Kg)</Text>
                       <View style={{width:"100%", marginTop:5, borderColor:"#efefef", borderWidth:1,borderRadius:5, padding:8}}>
                       <Text>2</Text>
                       </View>
                   </View>
                   <View style={{width:"100%", marginTop:5}}>
                       <Text style={{fontWeight:"bold"}}>Date to Send</Text>
                       <View style={{width:"100%", marginTop:5, borderColor:"#efefef", borderWidth:1,borderRadius:5, padding:8}}>
                       <Text>14-10-2021</Text>
                       </View>
                   </View>
                   <View style={{width:"100%", marginTop:5}}>
                       <Text style={{fontWeight:"bold"}}>Flexible Date</Text>
                       <View style={{width:"100%", marginTop:5, borderColor:"#efefef", borderWidth:1,borderRadius:5, padding:8}}>
                       <Text>+1 day</Text>
                       </View>
                   </View>
                   <View style={{width:"100%", marginTop:15}}>
                   <View style={{width:"100%", marginTop:5}}>
                   <Text style={{fontWeight:"bold"}}>Where I receive this parcel?</Text>
                       <Input
            placeholder='WxHxD'
            // label="WxHxD"
            // labelStyle={styles.labelStyle}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            // value={dimension}
            // onChangeText={(dimension) => { this.setState({ dimension }) }}
          />
                   </View>
                   <View style={{width:"100%", marginTop:5}}>
                   <Text style={{fontWeight:"bold"}}>Date</Text>
                   <Input
            placeholder='Date'
            inputStyle={{ color: '#0D3B66' }}
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            rightIcon={{ type: 'font-awesome', name: 'calendar', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
            value={date}
            onFocus={this.showDatePicker}
          />
          {
            //date picker
          }
          <DateTimePickerModal
            isVisible={this.state.isDatePickerVisible}
            mode="date"
            onConfirm={this.handleConfirmDatePicker}
            onCancel={this.hideDatePicker}
          />
                   </View>
                   
                    <View style={{width:"100%", marginTop:5}}>
                   <Text style={{fontWeight:"bold"}}>Time</Text>
                   <Input
                        placeholder='Time'
                       
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'clock-o', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={time}
                        onFocus={this.showTimePicker}
                    />
                   
                   {
                        //date picker
                    }
                   
                    {
                        //time picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isTimePickerVisible}
                        mode="time"
                        onConfirm={this.handleConfirmTimePicker}
                        onCancel={this.hideTimePicker}
                    />
                   </View>
                   <View style={{width:"100%", marginTop:5}}>
                   <Text style={{fontWeight:"bold"}}>My Delivery Price</Text>
                       <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            errorStyle={styles.errorStyle}
            // value={dimension}
            // onChangeText={(dimension) => { this.setState({ dimension }) }}
          />
                   </View>
                   </View>


                    </View>
                        
                    </View>
                 
                  </View>


                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    tripHeader: { backgroundColor: '#EAF5FF', padding: 10, flexDirection: 'row', justifyContent: 'space-between' },
    row: { flexDirection: 'row', alignItems: 'center' },
    heading: { fontSize: 18, fontWeight: 'bold', margin: 10, color: '#292929' },
    date: { color: '#878787', margin: 10 },
    editIcon: { margin: 10 },
    matchesContainer: { backgroundColor: "#F7F7F7", width: 114, textAlign: 'center', alignSelf: 'flex-end', margin: 10, padding: 5, color: '#0D3B66' },
    b1: { width: (width-50) / 2, height: 30, justifyContent: 'center', marginTop: 10 },
    input: { backgroundColor: '#F4F6FB', width: '100%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle: { borderBottomWidth: 0 },
    labelStyle: { backgroundColor: '#fff', color: '#000', fontWeight: '300' },
});
export default reviewOrderDetails
