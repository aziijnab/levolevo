import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar, Image, Dimensions, TouchableOpacity, ScrollView } from "react-native";
import { Icon, ListItem, Avatar } from 'react-native-elements'

import Header from '../components/header'

const { width, height } = Dimensions.get('screen')
class paymentRecevided extends Component {
    constructor(props)
    {super(props)
        this.state={
            payment:[1,2,3,4,5,6,7,8,9]
        }
    }
    render() {
        const payment=this.state.payment
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header title="Payment Recevied" back={this.props.navigation} />
                <ScrollView style={{ margin: 10 }}>
                    {payment.map((l,i)=>(
  <ListItem bottomDivider key={i}>
  <ListItem.Content>
      <View style={styles.row}>
          <Text style={styles.amount}>$500.00</Text>
          <Text style={styles.date}>22 Now 2021</Text>
      </View>

      <ListItem.Title style={{ fontSize: 18 }}>Lorem Lipsum lipra</ListItem.Title>
      <ListItem.Subtitle>Lorem Lipsum lipra</ListItem.Subtitle>
  </ListItem.Content>
</ListItem>
                    ))}
                  
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    row: { flexDirection: 'row', justifyContent: 'space-between', width: width - 50, marginBottom: 10 },
    amount: { color: '#F95738', fontSize: 18 },
    date: { color: '#939393', fontSize: 13 }
});
export default paymentRecevided