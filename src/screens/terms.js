import React, { Component } from "react";
import { StyleSheet, View, StatusBar, ActivityIndicator, Dimensions, ScrollView } from "react-native";
import { Icon, ListItem, Avatar, Button,Image } from 'react-native-elements'

import Header from '../components/header'

const { width, height } = Dimensions.get('screen')
class Terms extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />

                <Header title="Terms of use" back={this.props.navigation} />
<ScrollView>
<ListItem>
    <ListItem.Content>
        <ListItem.Title style={{fontSize:18}}>
            Welcome to LevoLevo
            </ListItem.Title>
            <ListItem.Subtitle style={{fontSize:16}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dui vel eu nunc, cras sagittis. Libero urna leo ultrices sed integer consectetur. Fermentum lorem dui proin vulputate in nulla. Ut sed tortor semper vitae. Aliquet suspendisse in mauris faucibus massa facilisis interdum semper. Tellus ornare feugiat leo ipsum dui fermentum volutpat. Lobortis curabitur dui integer faucibus purus nec. Ultricies semper donec sed donec blandit morbi. Eu sit suscipit massa vel. Pharetra pharetra, risus eget posuere libero. Id at auctor augue nulla vitae massa enim proin. Quam a vel aliquet adipiscing.
            </ListItem.Subtitle>
            <ListItem.Subtitle style={{fontSize:16}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dui vel eu nunc, cras sagittis. Libero urna leo ultrices sed integer consectetur. Fermentum lorem dui proin vulputate in nulla. Ut sed tortor semper vitae. Aliquet suspendisse in mauris faucibus massa facilisis interdum semper. Tellus ornare feugiat leo ipsum dui fermentum volutpat. Lobortis curabitur dui integer faucibus purus nec. Ultricies semper donec sed donec blandit morbi. Eu sit suscipit massa vel. Pharetra pharetra, risus eget posuere libero. Id at auctor augue nulla vitae massa enim proin. Quam a vel aliquet adipiscing.
            </ListItem.Subtitle>
    </ListItem.Content>
</ListItem>
</ScrollView>
        
               
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: { flex: 1, backgroundColor: '#fff' },


});
export default Terms