import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View, StatusBar, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { Button, Image, Input } from 'react-native-elements'
import Header from '../components/header'

const { width, height } = Dimensions.get('screen')

import { AddTrip, getAirports } from '../backend/apis'
import DropDownPicker from 'react-native-dropdown-picker';

import AsyncStorage from '@react-native-community/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";

class addTrip extends Component {
    constructor(props) {
        super(props)
        this.state = {
            days: '!1 days',
            image: '',
            origin: 'Rawalpindi',
            dest: 'Lahore',
            date: '28:01:2021',
            dateMax: '29:01:2021',
            description: 'Short description',
            prodImage: '',
            weight: '32',
            dimension: '20*30*20',
            country: 'uk',
            airports: [],
            token: '',
            userId: '',
            date: new Date(),
            showDate: false,
            showTime: false,
            time: new Date(),
        }

    }

    async componentDidMount() {
        const token = await AsyncStorage.getItem('@token');
        if (token != null) {
          ; this.setState({ token: token }) }
        else { }
        const userId = await AsyncStorage.getItem('@userId');
        if (userId != null) {  console.log(userId); this.setState({ userId: userId }) }
        else { }
        try {
            getAirports().then(data => {
                //  
                if (typeof data == 'object') {
                    //alert(data.id)
                    let tempAirport = []
                    for (let i = 0; i < data.airports.length; i++) {
                        tempAirport.push({
                            label: data.airports[i].City,
                            value: data.airports[i].City,
                            IATA: data.airports[i].IATA,
                        })
                    }
                    this.setState({
                        airports: tempAirport
                    })
                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            alert('=')
            // console.log(e)
        }
    }

    addTrip = () => {


        var tripData = {
            origin: this.state.origin,
            dest: this.state.dest,
            date: this.state.date,
            time: this.state.time,
        }
        let payLoad = {
            uId: this.state.userId,
            token: this.state.token
        }


        try {
            AddTrip(tripData, payLoad).then(data => {
                //  
                if (typeof data == 'object') {
                    //alert(data.id)
                    //  alert('Successfully login ')
                    this.props.navigation.navigate('myTrips',{value:'x'})
                    alert('trip added')


                }
                else {
                    alert(data)
                }
            })
        } catch (e) {
            alert('=')
        }

    }
    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true })
    };
    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false });
    };
    handleConfirmDatePicker = (date) => {
        let x = date
        console.log(x,'___')
        var date = x.toISOString().slice(0, 10);
        this.setState({
            date: date
        })
        this.hideDatePicker();
    };

    showTimePicker = () => {
        this.setState({ isTimePickerVisible: true })
    };
    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };
    handleConfirmTimePicker = (date) => {
        let x = date.toTimeString().substr(0, 5)
        this.setState({ time: x })
        this.hideTimePicker();
    };
    render() {
        var { origin, dest, date, dimension, date, showDate, showTime, time } = this.state
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <Header title="Trips" back={this.props.navigation} />

                <ScrollView>
                    <Text style={styles.secondLabel}>From</Text>
                    <DropDownPicker
                        items={this.state.airports}
                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        placeholder={'From'}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {
                            this.setState({
                                origin: item.IATA
                            }, () => {
                            })
                        }}
                        searchable
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                    <Text style={styles.secondLabel}>To</Text>
                    <DropDownPicker
                        items={this.state.airports}
                        containerStyle={{ width: '90%', alignSelf: 'center' }}
                        style={{ backgroundColor: '#F4F6FB' }}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        placeholder={'To'}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => {
                            this.setState({
                                dest: item.IATA
                            })
                        }}
                        searchable
                        dropDownMaxHeight={200}
                        labelStyle={{ fontSize: 14, textAlign: 'left', color: '#000' }}
                    />
                    <Input
                        placeholder='Date'
                        label="Date"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'calendar', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={date}
                        onFocus={this.showDatePicker}
                    />
                    <Input
                        placeholder='Time'
                        label="Time"
                        labelStyle={{ backgroundColor: '#fff' }}
                        inputStyle={{ color: '#0D3B66' }}
                        containerStyle={styles.input}
                        inputContainerStyle={styles.inputContainerStyle}
                        errorStyle={styles.errorStyle}
                        rightIcon={{ type: 'font-awesome', name: 'clock-o', size: 20, color: '#0D3B66', style: { marginRight: 10 } }}
                        value={time}
                        onFocus={this.showTimePicker}
                    />
                   
                   {
                        //date picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisible}
                        mode="date"
                        onConfirm={this.handleConfirmDatePicker}
                        onCancel={this.hideDatePicker}
                    />
                    {
                        //time picker
                    }
                    <DateTimePickerModal
                        isVisible={this.state.isTimePickerVisible}
                        mode="time"
                        onConfirm={this.handleConfirmTimePicker}
                        onCancel={this.hideTimePicker}
                    />


                    <Button
                        title="Save"
                        buttonStyle={{ backgroundColor: '#F95738', margin: 20 }}
                        onPress={this.addTrip}
                    />
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff' },
    header: { backgroundColor: '#fff' },
    input: { backgroundColor: '#F4F6FB', width: '90%', alignSelf: 'center', marginTop: 16 },
    errorStyle: { margin: 0, padding: 0, height: 0 },
    inputContainerStyle: { borderBottomWidth: 0 },
    labelStyle: { backgroundColor: '#fff', color: '#000', fontWeight: '300' },
    pcikerLabel: { marginLeft: 25, fontSize: 15, marginTop: 16 },
    uploadButton: { position: 'absolute', bottom: 10, backgroundColor: 'rgba(0,0,0,.5)', padding: 5, borderRadius: 10 },
    secondLabel: { fontSize: 18, marginLeft: '6%', marginTop: 16, fontWeight: '300' }

});
export default addTrip